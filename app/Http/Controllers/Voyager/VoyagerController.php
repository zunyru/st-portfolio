<?php

namespace App\Http\Controllers\Voyager;

use App\Article;
use App\Portfolio;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;
use Voyager;

class VoyagerController extends BaseVoyagerController
{
    public function index()
    {

        $all_port = Portfolio::count();
        $ports = Portfolio::paginate(5)->sortBy('created_at');
        $all_article = Article::count();
        $articles = Article::paginate(5)->sortBy('created_at');

        return Voyager::view('voyager::index', compact('all_port', 'ports', 'all_article', 'articles'));
    }
}
