-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 12, 2019 at 02:13 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `port_voyager`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `image`, `seo_title`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(1, 'dsdsd', '<h2 style=\"margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">What is Lorem Ipsum?</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><img src=\"http://127.0.0.1:8000/images/651466492.jpg\" style=\"width: 25%; float: right;\" class=\"note-float-right\"></p><h2 style=\"margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Why do we use it?</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 'articles\\August2019\\8fpkXJxj9HbwHljQRDRf.jpg', 'ddd', 'ddd', '2019-08-20 07:26:00', '2019-08-24 01:25:36', NULL, 'dsdsd'),
(2, 'Blognoe', '<div id=\"Content\" style=\"margin: 0px; padding: 0px; position: relative; font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: center;\"><div id=\"Translation\" style=\"margin: 0px 28.7969px; padding: 0px; text-align: left;\"><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px;\">The standard Lorem Ipsum passage, used since the 1500s</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px;\">1914 translation by H. Rackham</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px;\">1914 translation by H. Rackham</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">\"On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains.\"</p><div><br></div></div></div>', 'articles\\August2019\\0VQGgNcrnIICSPwEtDdJ.jpg', 'dredt43t', '\"On the other hand, we denounce with righteous indignation and dislike me', '2019-08-21 06:37:34', '2019-08-21 06:37:34', NULL, 'blognoe'),
(3, 'edggfdfdf', '<p>fdfdfdfddfd</p><p><br></p><p>bnbfadgafdg</p><p><img src=\"http://127.0.0.1:8000/images/900382827.jpg\" style=\"width: 1333.49px;\"><br></p>', 'articles\\August2019\\9TngAG6K0ycQAWroQbWC.jpg', 'aaa', 'aaa', '2019-08-24 01:31:04', '2019-08-24 01:31:04', NULL, 'edggfdfdf');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(22, 4, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'body', 'summernote', 'Content', 1, 0, 1, 1, 1, 1, '{}', 4),
(25, 4, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 7),
(27, 4, 'description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(30, 4, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 1, 1, 0, 1, '{}', 10),
(31, 5, 'id', 'number', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 1, 1, 1, 1, 1, '{}', 4),
(34, 5, 'body', 'summernote', 'Body', 1, 1, 1, 1, 1, 1, '{}', 5),
(35, 5, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 6),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 0, '{}', 7),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 0, 0, 1, 1, 1, 1, '{}', 9),
(38, 5, 'meta_keywords', 'text', 'Meta Keywords', 0, 0, 1, 1, 1, 1, '{}', 8),
(39, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 10),
(40, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(41, 5, 'seo_title', 'text_area', 'Seo Title', 0, 0, 1, 1, 1, 1, '{}', 3),
(42, 6, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(43, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(44, 6, 'body', 'summernote', 'Body', 1, 0, 1, 1, 1, 1, '{}', 4),
(45, 6, 'image', 'image', 'Image', 1, 1, 1, 1, 1, 1, '{}', 5),
(46, 6, 'seo_title', 'text', 'Seo Title', 0, 0, 1, 1, 1, 1, '{}', 6),
(47, 6, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 7),
(48, 6, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 8),
(49, 6, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 9),
(50, 6, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 10),
(51, 6, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 0, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(52, 4, 'seo_title', 'text', 'Seo Title', 0, 1, 1, 1, 1, 1, '{}', 6),
(53, 4, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 0, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController', '', 1, 0, NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(4, 'portfolios', 'portfolios', 'Portfolio', 'Portfolios', NULL, 'App\\Portfolio', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-20 06:04:01', '2019-08-21 06:33:19'),
(5, 'pages', 'pages', 'Page', 'Pages', NULL, 'App\\Page', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-20 07:15:45', '2019-08-20 07:20:17'),
(6, 'articles', 'articles', 'Article', 'Articles', NULL, 'App\\Article', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2019-08-20 07:24:14', '2019-08-21 06:32:33');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-08-20 05:11:33', '2019-08-20 05:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-08-20 05:11:33', '2019-08-20 05:11:33', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2019-08-20 05:11:33', '2019-08-20 05:11:33', 'voyager.hooks', NULL),
(12, 1, 'Portfolios', '', '_self', NULL, NULL, NULL, 15, '2019-08-20 06:04:01', '2019-08-20 06:04:01', 'voyager.portfolios.index', NULL),
(13, 1, 'Pages', '', '_self', NULL, NULL, NULL, 16, '2019-08-20 07:15:45', '2019-08-20 07:15:45', 'voyager.pages.index', NULL),
(14, 1, 'Articles', '', '_self', NULL, NULL, NULL, 17, '2019-08-20 07:24:14', '2019-08-20 07:24:14', 'voyager.articles.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `created_at`, `updated_at`, `seo_title`) VALUES
(1, 'About Us', 'dd', '<p><br></p><li style=\"outline-style: initial; outline-width: 0px; color: rgb(96, 96, 96); font-size: 12px;\"><div class=\"title\" style=\"outline-style: initial; outline-width: 0px; font-size: 24px; letter-spacing: 0.025em; color: rgb(0, 0, 0); margin-bottom: 30px;\">Every Individual Deserves a Website</div><p style=\"outline-style: initial; outline-width: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); font-size: 16px; letter-spacing: 0.025em; line-height: 1.5em;\">We believe every individual should have the power to create their own website or online store. If you can point and click, you can create a professional website or online store using our free and intuitive tools.</p></li><li style=\"outline-style: initial; outline-width: 0px; margin-top: 80px; color: rgb(96, 96, 96); font-size: 12px;\"><div class=\"title\" style=\"outline-style: initial; outline-width: 0px; font-size: 24px; letter-spacing: 0.025em; color: rgb(0, 0, 0); margin-bottom: 30px;\">Our Customers Mean the World</div><p style=\"outline-style: initial; outline-width: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); font-size: 16px; letter-spacing: 0.025em; line-height: 1.5em;\">At Website.com, we strive to provide exactly what our customers are looking for. A huge part of our brainstorming process is looking at our client feedback to make sure you\'re well taken care of.</p></li><li style=\"outline-style: initial; outline-width: 0px; margin-top: 80px; color: rgb(96, 96, 96); font-size: 12px;\"><div class=\"title\" style=\"outline-style: initial; outline-width: 0px; font-size: 24px; letter-spacing: 0.025em; color: rgb(0, 0, 0); margin-bottom: 30px;\">In-House Made</div><p style=\"outline-style: initial; outline-width: 0px; margin-bottom: 0px; color: rgb(0, 0, 0); font-size: 16px; letter-spacing: 0.025em; line-height: 1.5em;\">We are personally committed to delivering the very best. Everything, from customer support to product design and development, is provided by our dedicated (and adorable) team in beautiful BC, Canada.</p></li>', 'pages\\August2019\\QeNDzKFqxz1bRbX1116E.jpg', 'about-us', 'dd', 'dd', '2019-08-20 07:16:00', '2019-08-21 07:54:06', 'dd'),
(2, 'Contact Us', 'sfwefgewft', '<!--Facebook-->\r\n<a class=\"fb-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-facebook-f\"></i></a>\r\n<!--Twitter-->\r\n<a class=\"tw-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-twitter\"></i></a>\r\n<!--Google +-->\r\n<a class=\"gplus-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-google-plus-g\"></i></a>\r\n<!--Linkedin-->\r\n<a class=\"li-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-linkedin-in\"></i></a>\r\n<!--Instagram-->\r\n<a class=\"ins-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-instagram\"></i></a>\r\n<!--Pinterest-->\r\n<a class=\"pin-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-pinterest\"></i></a>\r\n<!--Vkontakte-->\r\n<a class=\"vk-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-vk\"></i></a>\r\n<!--Stack Overflow-->\r\n<a class=\"so-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-stack-overflow\"></i></a>\r\n<!--Youtube-->\r\n<a class=\"yt-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-youtube\"></i></a>\r\n<!--Slack-->\r\n<a class=\"slack-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-slack-hash\"></i></a>\r\n<!--Github-->\r\n<a class=\"git-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-github\"></i></a>\r\n<!--Comments-->\r\n<a class=\"comm-ic mr-3\" role=\"button\"><i class=\"fas fa-lg fa-comments\"></i></a>\r\n<!--Email-->\r\n<a class=\"email-ic mr-3\" role=\"button\"><i class=\"far fa-lg fa-envelope\"></i></a>\r\n<!--Dribbble-->\r\n<a class=\"dribbble-ic mr-3\" role=\"button\"><i class=\"fab fa-lg fa-dribbble\"></i></a>\r\n<!--Reddit-->\r\n<a class=\"reddit-ic\" role=\"button\"><i class=\"fab fa-lg fa-reddit-alien\"></i></a>', 'pages\\August2019\\Q85LMSULkHedQ3CbArHB.jpg', 'contact-us', 'sds', 'sds', '2019-08-21 08:05:00', '2019-08-24 01:03:29', 'dwrwsrwer');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(2, 'browse_bread', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(3, 'browse_database', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(4, 'browse_media', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(5, 'browse_compass', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(6, 'browse_menus', 'menus', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(7, 'read_menus', 'menus', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(8, 'edit_menus', 'menus', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(9, 'add_menus', 'menus', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(10, 'delete_menus', 'menus', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(11, 'browse_roles', 'roles', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(12, 'read_roles', 'roles', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(13, 'edit_roles', 'roles', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(14, 'add_roles', 'roles', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(15, 'delete_roles', 'roles', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(16, 'browse_users', 'users', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(17, 'read_users', 'users', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(18, 'edit_users', 'users', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(19, 'add_users', 'users', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(20, 'delete_users', 'users', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(21, 'browse_settings', 'settings', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(22, 'read_settings', 'settings', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(23, 'edit_settings', 'settings', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(24, 'add_settings', 'settings', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(25, 'delete_settings', 'settings', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(26, 'browse_hooks', NULL, '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(27, 'browse_portfolios', 'portfolios', '2019-08-20 06:04:01', '2019-08-20 06:04:01'),
(28, 'read_portfolios', 'portfolios', '2019-08-20 06:04:01', '2019-08-20 06:04:01'),
(29, 'edit_portfolios', 'portfolios', '2019-08-20 06:04:01', '2019-08-20 06:04:01'),
(30, 'add_portfolios', 'portfolios', '2019-08-20 06:04:01', '2019-08-20 06:04:01'),
(31, 'delete_portfolios', 'portfolios', '2019-08-20 06:04:01', '2019-08-20 06:04:01'),
(32, 'browse_pages', 'pages', '2019-08-20 07:15:45', '2019-08-20 07:15:45'),
(33, 'read_pages', 'pages', '2019-08-20 07:15:45', '2019-08-20 07:15:45'),
(34, 'edit_pages', 'pages', '2019-08-20 07:15:45', '2019-08-20 07:15:45'),
(35, 'add_pages', 'pages', '2019-08-20 07:15:45', '2019-08-20 07:15:45'),
(36, 'delete_pages', 'pages', '2019-08-20 07:15:45', '2019-08-20 07:15:45'),
(37, 'browse_articles', 'articles', '2019-08-20 07:24:14', '2019-08-20 07:24:14'),
(38, 'read_articles', 'articles', '2019-08-20 07:24:14', '2019-08-20 07:24:14'),
(39, 'edit_articles', 'articles', '2019-08-20 07:24:14', '2019-08-20 07:24:14'),
(40, 'add_articles', 'articles', '2019-08-20 07:24:14', '2019-08-20 07:24:14'),
(41, 'delete_articles', 'articles', '2019-08-20 07:24:14', '2019-08-20 07:24:14');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1);

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `title`, `body`, `image`, `seo_title`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(3, 'What is Lorem Ipsum?', '<p><br></p>', 'portfolios\\August2019\\BHB7apSYsLNZSNSA3Cnd.jpg', NULL, 'Lorem Ipsum Description434', '2019-08-20 07:02:00', '2019-08-24 01:02:16', NULL, 'design'),
(4, 'Why do we use it?', '<h2 style=\"margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px; color: rgb(0, 0, 0);\">Why do we use it?</h2><p><span style=\"font-family: &quot;Open Sans&quot;, Arial, sans-serif; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</span><br></p>', 'portfolios\\August2019\\AlIKKarKA4Z3mRggoXVc.jpg', 'Why do we use it?', 'Why do we use it?', '2019-08-21 05:09:32', '2019-08-21 05:09:32', NULL, 'design-2'),
(5, 'my portfolio', '<div style=\"margin: 0px 14.3906px 0px 28.7969px; padding: 0px; width: 436.797px; float: left; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><h2 style=\"margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">What is Lorem Ipsum?</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p></div><div style=\"margin: 0px 28.7969px 0px 14.3906px; padding: 0px; width: 436.797px; float: right; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\"><h2 style=\"margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 24px; font-family: DauphinPlain; font-size: 24px;\">Why do we use it?</h2><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify;\">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p><div><br></div></div>', 'portfolios\\August2019\\bPJU4uN05pvLrEd6esUv.jpg', 'ww3', 'Lertgtr6c', '2019-08-21 05:11:14', '2019-08-21 05:11:14', NULL, 'portfolio-2'),
(6, 'Portfolio Super', '<h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">The standard Lorem Ipsum passage, used since the 1500s</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.32 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">1914 translation by H. Rackham</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?\"</p><h3 style=\"margin: 15px 0px; padding: 0px; font-weight: 700; font-size: 14px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">Section 1.10.33 of \"de Finibus Bonorum et Malorum\", written by Cicero in 45 BC</h3><p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: &quot;Open Sans&quot;, Arial, sans-serif;\">\"At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.\"</p>', 'portfolios\\August2019\\jiS6RKPzQcgMghdO12qZ.jpg', 'ddd', 'trwui p9p9-pp9', '2019-08-21 05:12:38', '2019-08-21 05:12:38', NULL, 'portfolio-3'),
(7, 'ygy', '<p>yy6ty6</p>', 'portfolios\\August2019\\iOMWZOvWUyIzIPeCxYtg.png', '666', '666', '2019-08-22 09:54:08', '2019-08-22 09:54:08', NULL, 'ygy'),
(8, 'p[p[p', '<p>Thidsdssss</p><p><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QfERXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAMAAAExAAIAAAAiAAAAcgEyAAIAAAAUAAAAlIdpAAQAAAABAAAAqAAAANQABFNJAAAnEAAEU0kAACcQQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpADIwMTg6MDM6MzEgMDE6Mzg6MTcAAAOgAQADAAAAAf//AACgAgAEAAAAAQAAAfSgAwAEAAAAAQAAAZAAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAGigAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAIAAoAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/AOxSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//9DsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJPDtofB2EwHRpPhuSUskkkkpSSSSSlJJJJKUkkkkp//9HsUkkklKSSSSUpJJIkDU8JKUkjU4WTdqxhDf3n+0f+S/6Kus6XRW3fk2bgOddjR/37/pJKc1oc521gLnfutBJ/BW6el5L9bCKh5+533N9v/SR3dSwMduzHAfHZkBs/11Uu6nkW6B4qb4MOv+f9L/NSU3fs3TsOHXEOd2L/AHH+zWP/ACCI815+G/0udQ0HQhzTLf8AOWLubJMiTyZ1V7pOQ1t7qZEWCWj+U3/zD/qElNIGRKSPn1CjKe3QNf72/B30v+mq+5viPvSUukkkkpSSSSSlJJJJKf/S7FJJJJSkkkklK417d45Ww2rBxKftAbuAAIsjc4z9GP3eVjrU6e5uRhPxn/myz+y76BSUxHVH23111sDGOeAS7V0E+XtarOTm047mssa5xcNwgTwsnHa5uXUx30m2BrviDBVnq/8AP1f1D+UJKbH7Wxf3X/5o/vS/a2L+6/8AzR/eg4fTqraG22ky8S0NMQP/ACSqZVBx73VTuAgtJ5IPikp0f2ti/uv/AM0f3pftbF/df/mj+9ZKk+uxjWOeIFo3M+ASU6n7Wxf3X/cP/JI+Nk15LXOrBAadp3CO0/xWEtTo/wDM2f1/++tSU5TfohOmb9EJ0lKSSSSUpJJJJT//0+xSSSSUpJJJJSlZ6fd6WU2fo2ew/E/Q/wCkqyRnsYPY+BSU6GXT6fUqbAPba5p/tAw7/o7VHq/8/V/VP5QrgAzKKLho5rmv+BB22N/6tU+r/wA/V/UP5QkpHjdQux6/TDQ9o+jJIIn7/aq9tr7bHWWGXO5jQeTQp4+PZkW+mzQDV7+zR/r9FaVvS8d1QbX+je0QH8z/AMYPz0lNHBxftN3uH6Jmr/M9q/8Aya1cnGryWBlkiDILdCnx6GY9TamcDk9yfznIiSnEzsduPkbGfQc0Obyf5LtSrnR/5mz+v/31qXV691LLR/g3Qfg7T/qtiXR/5mz+v/31qSnKb9EJ0zfohOkpSSSSSlJJJJKf/9TsUkkklKSSSSUpJJJJTo9Iu/nKD/Xb/wBS9LqlN1l1Zrrc8BpBI+Ko02vptbaz6TZ0PBnSCrX7XyP3GfikpVFnUcev068fSZJLSSSe7veifa+q/wCgH+af/SiH+1sj9xn4pftfI/cZ+KSkn2vqv+gH+af/AEol9r6r/oB/mn/0oh/tfI/cZ+KX7Wyf3Gfikpe27qd1bqn0e14gw0z8verHS6rK6rBYwsJfIB8NrVW/a+R+4z8Uv2vkfuM/FJTRb9EJ0gIEJJKUkkkkpSSSSSn/1exSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//9bsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//Z/+0P1FBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQAAAAAAAAAAAAAAAAAAAAADhCSU0EOgAAAAAA5QAAABAAAAABAAAAAAALcHJpbnRPdXRwdXQAAAAFAAAAAFBzdFNib29sAQAAAABJbnRlZW51bQAAAABJbnRlAAAAAENscm0AAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAAAAAD3ByaW50UHJvb2ZTZXR1cE9iamMAAAAMAFAAcgBvAG8AZgAgAFMAZQB0AHUAcAAAAAAACnByb29mU2V0dXAAAAABAAAAAEJsdG5lbnVtAAAADGJ1aWx0aW5Qcm9vZgAAAAlwcm9vZkNNWUsAOEJJTQQ7AAAAAAItAAAAEAAAAAEAAAAAABJwcmludE91dHB1dE9wdGlvbnMAAAAXAAAAAENwdG5ib29sAAAAAABDbGJyYm9vbAAAAAAAUmdzTWJvb2wAAAAAAENybkNib29sAAAAAABDbnRDYm9vbAAAAAAATGJsc2Jvb2wAAAAAAE5ndHZib29sAAAAAABFbWxEYm9vbAAAAAAASW50cmJvb2wAAAAAAEJja2dPYmpjAAAAAQAAAAAAAFJHQkMAAAADAAAAAFJkICBkb3ViQG/gAAAAAAAAAAAAR3JuIGRvdWJAb+AAAAAAAAAAAABCbCAgZG91YkBv4AAAAAAAAAAAAEJyZFRVbnRGI1JsdAAAAAAAAAAAAAAAAEJsZCBVbnRGI1JsdAAAAAAAAAAAAAAAAFJzbHRVbnRGI1JsdEC0QAAAAAAAAAAACnZlY3RvckRhdGFib29sAQAAAABQZ1BzZW51bQAAAABQZ1BzAAAAAFBnUEMAAAAATGVmdFVudEYjUmx0AAAAAAAAAAAAAAAAVG9wIFVudEYjUmx0AAAAAAAAAAAAAAAAU2NsIFVudEYjUHJjQFkAAAAAAAAAAAAQY3JvcFdoZW5QcmludGluZ2Jvb2wAAAAADmNyb3BSZWN0Qm90dG9tbG9uZwAAAAAAAAAMY3JvcFJlY3RMZWZ0bG9uZwAAAAAAAAANY3JvcFJlY3RSaWdodGxvbmcAAAAAAAAAC2Nyb3BSZWN0VG9wbG9uZwAAAAAAOEJJTQPtAAAAAAAQAEgAAAACAAIASAAAAAIAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAWjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAAjhCSU0EAgAAAAAABgAAAAAAADhCSU0EMAAAAAAAAwEBAQA4QklNBC0AAAAAAAYAAQAAAAM4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADSQAAAAYAAAAAAAAAAAAAAZAAAAH0AAAACgBVAG4AdABpAHQAbABlAGQALQAxAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAH0AAABkAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAABAAAAABAAAAAAAAbnVsbAAAAAIAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABkAAAAABSZ2h0bG9uZwAAAfQAAAAGc2xpY2VzVmxMcwAAAAFPYmpjAAAAAQAAAAAABXNsaWNlAAAAEgAAAAdzbGljZUlEbG9uZwAAAAAAAAAHZ3JvdXBJRGxvbmcAAAAAAAAABm9yaWdpbmVudW0AAAAMRVNsaWNlT3JpZ2luAAAADWF1dG9HZW5lcmF0ZWQAAAAAVHlwZWVudW0AAAAKRVNsaWNlVHlwZQAAAABJbWcgAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAZAAAAAAUmdodGxvbmcAAAH0AAAAA3VybFRFWFQAAAABAAAAAAAAbnVsbFRFWFQAAAABAAAAAAAATXNnZVRFWFQAAAABAAAAAAAGYWx0VGFnVEVYVAAAAAEAAAAAAA5jZWxsVGV4dElzSFRNTGJvb2wBAAAACGNlbGxUZXh0VEVYVAAAAAEAAAAAAAlob3J6QWxpZ25lbnVtAAAAD0VTbGljZUhvcnpBbGlnbgAAAAdkZWZhdWx0AAAACXZlcnRBbGlnbmVudW0AAAAPRVNsaWNlVmVydEFsaWduAAAAB2RlZmF1bHQAAAALYmdDb2xvclR5cGVlbnVtAAAAEUVTbGljZUJHQ29sb3JUeXBlAAAAAE5vbmUAAAAJdG9wT3V0c2V0bG9uZwAAAAAAAAAKbGVmdE91dHNldGxvbmcAAAAAAAAADGJvdHRvbU91dHNldGxvbmcAAAAAAAAAC3JpZ2h0T3V0c2V0bG9uZwAAAAAAOEJJTQQoAAAAAAAMAAAAAj/wAAAAAAAAOEJJTQQRAAAAAAABAQA4QklNBBQAAAAAAAQAAAADOEJJTQQMAAAAAAamAAAAAQAAAKAAAACAAAAB4AAA8AAAAAaKABgAAf/Y/+0ADEFkb2JlX0NNAAL/7gAOQWRvYmUAZIAAAAAB/9sAhAAMCAgICQgMCQkMEQsKCxEVDwwMDxUYExMVExMYEQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMAQ0LCw0ODRAODhAUDg4OFBQODg4OFBEMDAwMDBERDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCACAAKADASIAAhEBAxEB/90ABAAK/8QBPwAAAQUBAQEBAQEAAAAAAAAAAwABAgQFBgcICQoLAQABBQEBAQEBAQAAAAAAAAABAAIDBAUGBwgJCgsQAAEEAQMCBAIFBwYIBQMMMwEAAhEDBCESMQVBUWETInGBMgYUkaGxQiMkFVLBYjM0coLRQwclklPw4fFjczUWorKDJkSTVGRFwqN0NhfSVeJl8rOEw9N14/NGJ5SkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2N0dXZ3eHl6e3x9fn9xEAAgIBAgQEAwQFBgcHBgU1AQACEQMhMRIEQVFhcSITBTKBkRShsUIjwVLR8DMkYuFygpJDUxVjczTxJQYWorKDByY1wtJEk1SjF2RFVTZ0ZeLys4TD03Xj80aUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9ic3R1dnd4eXp7fH/9oADAMBAAIRAxEAPwDsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//Q7FJJJJSkkkklKSSSSUpJJJJSkkkklKSSTw7aHwdhMB0aT4bklLJJJJKUkkkkpSSSSSlJJJJKf//R7FJJJJSkkkklKSSSJA1PCSlJI1OFk3asYQ395/tH/kv+irrOl0Vt35Nm4DnXY0f9+/6SSnNaHOdtYC537rQSfwVunpeS/Wwioefud9zfb/0kd3UsDHbsxwHx2ZAbP9dVLup5FugeKm+DDr/n/S/zUlN37N07Dh1xDndi/wBx/s1j/wAgiPNefhv9LnUNB0Ic0y3/ADli7myTIk8mdVe6TkNbe6mRFglo/lN/8w/6hJTSBkSkj59Qoynt0DX+9vwd9L/pqvub4j70lLpJJJKUkkkkpSSSSSn/0uxSSSSUpJJJJSuNe3eOVsNqwcSn7QG7gACLI3OM/Rj93lY61OnubkYT8Z/5ss/su+gUlMR1R9t9ddbAxjngEu1dBPl7Wqzk5tOO5rLGucXDcIE8LJx2ubl1Md9Jtga74gwVZ6v/AD9X9Q/lCSmx+1sX91/+aP70v2ti/uv/AM0f3oOH06q2httpMvEtDTED/wAkqmVQce91U7gILSeSD4pKdH9rYv7r/wDNH96X7Wxf3X/5o/vWSpPrsY1jniBaNzPgElOp+1sX91/3D/ySPjZNeS1zqwQGnadwjtP8VhLU6P8AzNn9f/vrUlOU36ITpm/RCdJSkkkklKSSSSU//9PsUkkklKSSSSUpWen3ellNn6NnsPxP0P8ApKskZ7GD2PgUlOhl0+n1KmwD22uaf7QMO/6O1R6v/P1f1T+UK4AMyii4aOa5r/gQdtjf+rVPq/8AP1f1D+UJKR43ULsev0w0PaPoySCJ+/2qvba+2x1lhlzuY0Hk0KePj2ZFvps0A1e/s0f6/RWlb0vHdUG1/o3tEB/M/wDGD89JTRwcX7Td7h+iZq/zPav/AMmtXJxq8lgZZIgyC3Qp8ehmPU2pnA5Pcn85yIkpxM7Hbj5Gxn0HNDm8n+S7Uq50f+Zs/r/99al1evdSy0f4N0H4O0/6rYl0f+Zs/r/99akpym/RCdM36ITpKUkkkkpSSSSSn//U7FJJJJSkkkklKSSSSU6PSLv5yg/12/8AUvS6pTdZdWa63PAaQSPiqNNr6bW2s+k2dDwZ0gq1+18j9xn4pKVRZ1HHr9OvH0mSS0kknu73on2vqv8AoB/mn/0oh/tbI/cZ+KX7XyP3GfikpJ9r6r/oB/mn/wBKJfa+q/6Af5p/9KIf7XyP3Gfil+1sn9xn4pKXtu6ndW6p9HteIMNM/L3qx0uqyuqwWMLCXyAfDa1Vv2vkfuM/FL9r5H7jPxSU0W/RCdICBCSSlJJJJKUkkkkp/9XsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//W7FJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT//2ThCSU0EIQAAAAAAXQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABcAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAEMAIAAyADAAMQA3AAAAAQA4QklNBAYAAAAAAAcABAAAAAEBAP/hDnlodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTgtMDMtMzFUMDE6Mzg6MTcrMDM6MDAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTY4NDRiNWMtZTJkZC03YTQ1LThlYjMtYjdhM2I2M2JlNTkzIiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MDMyNjAyNTQtMzQ2Yi0xMWU4LThhNzEtZGM5NDQ4NjUyMTEyIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MzZmOTIyNGMtZWQ2Ny1lNjRlLTk1YjAtYmMyNDkwMGUyMTc4IiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBkYzpmb3JtYXQ9ImltYWdlL2pwZWciPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjM2ZjkyMjRjLWVkNjctZTY0ZS05NWIwLWJjMjQ5MDBlMjE3OCIgc3RFdnQ6d2hlbj0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo1Njg0NGI1Yy1lMmRkLTdhNDUtOGViMy1iN2EzYjYzYmU1OTMiIHN0RXZ0OndoZW49IjIwMTgtMDMtMzFUMDE6Mzg6MTcrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPHBob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPHJkZjpCYWc+IDxyZGY6bGk+YWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjY1NGY4NDlkLTM0NmEtMTFlOC04YTcxLWRjOTQ0ODY1MjExMjwvcmRmOmxpPiA8L3JkZjpCYWc+IDwvcGhvdG9zaG9wOkRvY3VtZW50QW5jZXN0b3JzPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAQwADAgICAgIDAgICAwMDAwQGBAQEBAQIBgYFBgkICgoJCAkJCgwPDAoLDgsJCQ0RDQ4PEBAREAoMEhMSEBMPEBAQ/9sAQwEDAwMEAwQIBAQIEAsJCxAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQ/8IAEQgBkAH0AwEiAAIRAQMRAf/EABsAAQEAAwEBAQAAAAAAAAAAAAABAgUGBAMH/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEAMQAAAB/YgSgBFAEKSgAAAlCUSgAAABKAAACUAJYKCUSgAAABFAAAAABKAAAAAJYUAAAAAAAAAAAhQCFAAAABATIJQAiiKJZQABKEoSiLBQAxyAAAAABjkGOQASwKIsFABKEoAgKCUAEoSiUAAAAEoSgACUAAJQAAASgBLCgigAAAACAoAAIoSiUAAAAGOQSgAAAAAAAABjkAEolCKAAAAAICglACAoIUAAY5ABLCywUAJQxyACVCgAAY5ABLBZRLBQAAAAgKAAAgoJZQAAAAQssKAAAmwPBl026OV3e0hymk7jhigAAASwqUSwoAAAAICgAAgKCWUAAAAY5QssKA2e6OW3XTZHh9zwnuw5nTHUe/g98dTwvdaA5gACUAJYLKJYUAAAAEBQAAEFBLBUoAAAZbU0/p6vZnObr1QNZozq9JzMPd4KAH2+A/RcNZuT85m404AIUCWFSiWFSgAAAEBQAACFASgA+h85vd2crut/T4/XD5HoxyGn0vZw4Sd6OCd4ODd6OCd6OCd6OW6ijWct3Y4N3sODw7/WHGAEKAQoAAAAICgAAAAllAG+0P1P0Fj8zzaTRU+nScv1BvvF7uGOpnEDt3EDt3EDuHDjuJxA7icQO4cOO3cQO3vD/Y/QdXtdWcYBKAEsKAAAACAoCUAIKQWCpRKOv2/Gdmcdquv48vUcv1BvuH7jhzwp9z5fftPWfm967kCpQAmZilAH2+P1P0HV7TVnGpRLCpRAqUAAAJSAmQYshjkEBccoJaShjkDv+A6M6Dgv0Dlzn+q5fqDfcP3HDmv3uk9x3DHIcB2fCmOQY2j79Xlsj84uy1xg+mJj9/l9T9B1e01ZxbIJYCmNCVRjkEoxyDFkICZBjkAEUMchJaSglD1eWH6N5PlsT846nTbk33D9xw5r8g9mw0Y+nxyGOQN/qu5MwfDP6DldH2XGE+/y+x+g6raas40CWCZDG2EyABKAMcggKAABAoJZQAADc9b+ed+aabrUm64fuOHPCAfcvp6v0n5zl0uBtPeAAGHAfoXIGo+3x+p+g6vaas40CWFSiWFAAAABAUAABBSFQUAADquV952/z+gcP3HDnhTIy7SbAAAAAAaTd/E/P/t8fqfoOr2mrONAlhUoQVKAAAAQFAABFAEoAAAJR3Xs5jpxw/ccgaj0Zj3PCPa8Q9zwj2vEPa8Q9t8I9t8I82f2+p22r2mrONAlEoJQAAAABAUEsoBAUEsCgAlCUfbvvzrojpWmG5aYblphuWmG5aYblphuWmG5aYblphuWmG51fy8JoAJYLKJYVKAAAAQFAAAIUEsoSgAAAhZYUAAAAAAAAAACWFAlhQAAAAQFAY5AEIZAlxyAAAAEuJlLiZAAMcgAAAAAAYmQEsFlEuJkAAAACAoAAAAAAAAAAEoAAAAAAAAAAAAAAAAAAAgKlCUAIKQqUJQAQpCkKgqUAJQlBCgAJQQpCgEKkMkFSgAhQAEFShBUogVAsFShKEFIWBYFQVBUFQVBUFQVBUFIVBZYVBYFQUhSFShABUoShKEFILjSpQlBCkKgsQySgBKEoQVBQEFQUhSFIVKIhklBCkKlCUgFlMaCykAmUAFlIUxthccoFgAAUQCykAAKQAFxygBZYJRKAFlhZYSqY1SA/8QAKBAAAQQBAwQDAQADAQAAAAAAAwABAgQFFBU0EBMzUBESIDAiQJBB/9oACAEBAAEFAv8AorGMpuLFnmg4+uFZULQJ7j/0NGyZCxQoqEIQbpcF3a7e2GIpUHEydw064espRgx8mCCrHawFXBOGx7JvmThx1gqDjAQTMzdTXK4EbKzd5kIXpiiu01lQ/I/YCrWDIOJihVwh6zJAbFyo4o12yb8hK4Ss7SYo2KN4vCXrIxlNw4w80HH1hfguQrCRsoaalKU3/eML96yygfof1Pyg0rJkLFDioDGJujv8Ma1aRY5Iy0dpaO2tHbWjtrR21o7a0dtaO2tHbWjtrHisgsK+Bz19JbWjtrR21o7amA4m9EMZCyDiiSQaVcHWUowYRhm/1slw/RYkv+XQ1yuFEyxJKZCFWI8CncrDlr6i19Na+otfTWvprX1Fr6i19Ra+otfTWvqLX1Fr6i19Ra+mtfTWvqJrtV3WS4fogk7RYu0mLD7j+Ph+mI8Cv8z+vx+A+dZLh+jxpu5XWSF2rHTEeBX+Z0ZpSd69iLf1D5lkuH6PGm7dhZILkB0xHgV/mIIZWCgrjrwV2lA0P08ZM35D51kuH6Nn+rhJ3hOzSYw+yZYjwK/zFifP1sRaJ/xXDKwWzUjOr+Q+dZLh+kxJf8Vlg/WaxHgV/mKmfTn+fllZsRrjd3d/xSq6YayAOzYi0pvKMoS6B86yXD9JVL2bCti71dliPAr/ADOgLZ66fLF+CFIaX4xlX5foQQzNAcINlh/BOgfOslw/S0C92srwezZxHgV/mfwrV5WSxjGEfxkR9yr0D5lkuH6XFm+hllRfYOI8Cv8AM/AASsFs0i13TM7vTrNWF+ZM0mJBxkQfMslw/Swm45wmxITgxIYuDjGr/M6hCSwSvXHWGrWMaSx1KUJfvKDeNlB86yXD9NizPMKaEWdX+Z0hCZZ1Ksao/wCeVH9q6D5lkuH6aiZw2et/mJmd3qVIVof0LBij+Pq4fOslw/T1Sd4HS/zEA8q891srdbK3ayt1srdbK3Wyt2sLdbK3awt1srdrC3Wyt2sLdbKlJ5yD51kuH6fEmZn6XK1idrSW1pLa0ltaS2tJbWktrSW1pLa0ltaS2tJbWktrSW1pLa0ltaS2tJbQqtlirJcP04CdkzO0m/1clw/UVckIYd1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqrl8Bwf9hf/8QAFBEBAAAAAAAAAAAAAAAAAAAAkP/aAAgBAwEBPwFIP//EABQRAQAAAAAAAAAAAAAAAAAAAJD/2gAIAQIBAT8BSD//xAA2EAABAgMCDQMCBgMAAAAAAAABAAIDEBESISAxMjM0QUJQUWFygZEiI3EwUgQTQJChsRQkYP/aAAgBAQAGPwL9xWjGl3wqxHBiBs2nDWU2M3buO+qtZZHFy951sqjGgfE3t5V3v7cMuVY77uDV6Id/EztPdQc0RD9bkIourJwpcbxvOgvPJBzvQOeNC363c1dOj338AqQG0HEr3Xl3zJ0Am7GJCMMbN4+iEfkr/Yfa5Be2wCdXuAHNey238qjn2RwbgtijUVUa06GcThRFh2TTdtGNLvhe77aBsWnDWcClu0eAREMWB/Kq9xd8/QsnGy6QigXP/vdYoyyDrKBivtqkNgb8TqqQPwbjzK9xj+y0dyzDlmHLMOWYcsw5ZhyzDlmHLMOXrguDXCRa0VcLwsw5ZhyzDlmHKsSEWjcdmGwuVY77PIL0srzM7TnUHNEw3WrJp+mfuN8HuJ0fEv4Be1Ds8yvdeXfKf1SLHxQCFn2rPtWfas+1Z9qz7Vn2rPtWfas+1Z9qz7Vn2rPtWfas+1Z9qAEYX3SfuNkX7SrQxFOYDSo1Ig4xN/VKJ2/RQ+oSfuQNJ9TLpW9USb+qUTtOjGl3wgTBdff9aH1CT9yWdUS6VWi9l839UonaQhs1/wAKwwd+Mi9gsvF92vDDiLjiOFD6hJ+5A4ar02L9wRadafC4G6T+qUTtJ/TgRGtuAdgiEO/wvymDIycKH1CT9yvgnZvEmxxtekyf1SidpBxyXXFVEi8m/VzRJxnBvve7HKoyYl6oxpd8IseKETh9Qk/crH1oMRk9nKT+qUTtOjHVHAq6CArcR9o4P+S/VkTpFYHDmqMaB8JsX7hScPqEn7mYdYuMnXUa68J/VKJ2+jYGIXuQa0UAwXXVLb5w+oSfuYwjtyERoyD/AAn9UonbBENuLaPBVymcZUF5Ks43G8nCLTrTof2mUPqEn7mbEGyaoPGIiqLDrCiwzsvIlE7YH5cPueCsM7njIv8Aw/pP28UY0ZlCLmj6Fv7xKH1CT9zmE45H9SJG1jlEmIcMVcVZxk3k/Ut0qWGUPqEn7nbwfccCJINbeTcuLjjP1XMOsIjhcofUJP3Qx/KcTtIvY1pOK9ZDFkMWQxZDFkMWQxZDFkMWQxZDFkMWQxZDFksRe7XeofUJP3Q6Add4m97IJIK0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dyhkwHXOEn7oZE4G/wCFUa/0z90iHGrVt1y2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vCdCZWp/dFx/8D//xAAnEAABBAEEAQUBAQEBAAAAAAABABARISAwMUFRgUBhobHwcZFQ0f/aAAgBAQABPyHI6I/4I9DDRnGAUYQ0PChQ8KFDQoUKFCjKFzpw8KMO8ChgMBlzo84DR4xC4cYBcaHeBQwGAy50eW5YNzmMDwguHGAXDFcveR0RON95WrVvatg9q1atW3GpeV6BePUjR4xDR6GGOiMYxhowjKGjGPQx6OMY/wCJGmUMBgFS4RalyqelSp+VThU1ZcKnKC4cY8MWrVGA9GNHjEaPGoUNANxpW/OAe8uMChpW96BxGXD+c/OHlwiQNyjIMEEH3orzlxiNXzh3gUMA4Qcvzo8vJFC4AlBAAEbbkL/eM0AiBwec+MQuHGAXDHSOnxq8ALJoRZQEWHopDpJ7VAIKERQAQ462RASJiEUgHbLjEeh7wOyGAcIMUX5ykDdEItokCggQEQk9ygSCAjeLnIAuShBZAqgbIakqEdFqXz17ZDbA8ILhxgFwx0ih6IICeIBJQcAFQHISIyTsQCAAAqnIEGIneKKAYI5/8R6eXgqCFK8SEZ+WFPpIq5GmPSn0BIHKICaCY2AEASf8Stw87kCy5CLrJKEOAMXgAgv+JFFkmybJO7hENsU+46QyeQAIW4iCW8IQjjxiNaczojGbK5ASpUkA/wBJX+iB2gAGJAFo+SgGN1QZnoHdJALaSnM3SgjWm5kwxIFFBI9WnGWlpeWlEBud9kRI9rbEI8WIcCgowLoIbhSECYEwNygDCf4o+DGHFQgPbSCIiIiIjSgxPALTY2BzFoYUREMmQxJ7xlpaXl50yhgHCCDI9CghPI95QSsj7peOoXJQg8Ukx36b9/7geEFw49CdGI7YgNFFSDATvFTAGBEdJRqenBUF8ouFvFuB4dq/cXoP3HJmam/ccmagY/WSgOy30vVHEOEUZIAJjeOkFlkEgox5MwncFIAMhBneX+UYRt/gUDoKB0FA6CgdBQOgoHQUDoKB0FA6CgdBQOgoHQUDoKHQUDoKB0FA6CEbIY+piFw4wC41ChgHCiQR2gTrbYpR2pEVb/KN+z2c6IhZASYUeEOwTSBF5DQ4V9LELhx6E6IRBnsS9ixWITR7hAzYb5Rv2ezUTGxJ2BCez3LcmDDCsH+F8HYj3xCJVG+bHOhX0n5YaAzpUqVKmL01MFSCLuowEoYIRAYQyZAEFHIAxcNSFS+Ub530VL477wB9IADpUqVNym2fSVfkJ9pCHvR5CpUqVNQr6SpUuWENTU9INTUq0DkMTkkTL+AYEmRlfKN876NO6PFoAAkg2IYfKQgeyLnJZJ98LJAAJJMADkoURiwfXs00KKnQKlguglc/HjrGn0sRr2rVq1bFBWra3tBWj3MPhQIOyBBiSRE8FSI9+V8o3zvorUdoKZmRNsI4gcOxJmFNFwTsArVq+1faq2qe73cdEBkDgUDgYECEIo8GJNUCr7VvT6StWxQlcK2t7QVwjKMq1fegcQ4QYiRCDOCGrghjADifZfKN+z20QKuInQ6QwoMADgY1jaPsgZAIw59LELhxgFxqFDAYBxbOGR/Q1tpu9k+Ub9nth5QgDaQcEdCDPAGyP6pB/wDUCDJEADclAHzCLXgFiQBBlFNuQdU/CvpYhcOMBj50Dp8LcaBQxIW0OgW0cYFWIFs/Z7YAAPcmwIaH7k3LtRNHYqrcMngSmrORxgcBK5AL4BGFPpeqKGAzKmEk67YJeCU+4t9P6f6wN91EZ89HQOEGEQP8D8K+liMB6E5hhhcAG9/eHyh9MPokwA7UaHb9zlDQjgEoEGF7IkRMkzJqEWvpPz6coYDAYGYqiLn3QeVIA3y/7vZj5EP4r9pX7SvwFftK/aV+0r8pX7SvylftK/KV+0r8pX6yozJOUCBL0+lgUFw4wGqUMBokdI8d47HQRsa9FEREREREREREB5AQT0G+liNaFChQ0MUGhgHhBQiGhEAkgCpugSaQEgvLSpaSpKlSWlpeVL/SaHC4eHhBoaFGgdEYmCIOxVAisEyPTZmZmZmZmZmZmZmbDPiRHL8sNI6ZxDh+Htc5W/Ktw95cYjAINw3GF6B0QpwnKVOEuHnU4zGM5WxQwD296VvzgHtrw4VuUMA9vbWra1atWrYq2tWre0FatWrVq1atWrVq1atWrVq0JVq1atWrVq1atW4lrVq1bWgrVtatTmcRpc6oe8uMR/xp/wCRatWrVvatWrVq2trVtatWrVq1atWrVq1atWrVq1atWrVq1atW9q1atWra2tW1q1a8ryvK8q+2OPlBBWgvLFeV5x8ryvK8v5V9sFfbeV5XleV5V9rjdeW5YLjK0FxuvKK8rzoHEYBuHjOH5UOHjLjELjMLjCNA5hhj5z86nnPnX86BeFGlWdZh6yisRq00qVIUqWLyqUvKClS0hS0qmlSpUhpUqVKClpUhSpUqVIUqcZVKXlBSpaVLQVCtoYveAUFBW9qPdoeGj3whQWChoUFQVBUFQrUNywXCvGCg1tag6A5hh6/lhr//2gAMAwEAAgADAAAAEPNNHPOPKNPNNOMPMNMMMPPONHPPPOPMPPFPAPKPKPPKPPPPFNFNPPKPOAPFPAONPPPPFOPPLPPHPPDPLHFLDHOPPNNPLPPLPPPHHPPPLOPHNLLPLPPLLPPHPPPPPPNLPPPPDPKPKDHHHLDPDPBLPPOHLBPHPKLLPPPPBPOPLOPNPPKPOPEIPPOPLFPPPPHKPPHPBPLPDOHLPPPPELOOPPKPPBPHPDLPPPHPAPLPDLHPPOLGJGNFPPLPLBPHPDLPPPPPFPPPOPPNCODNDPCLHPLPPHPPPPPPDPHPILKPEDFDEKHMPPPDIOIPOEHFLPHPPPPPFPOPMDNHGAAAAAAAAECPPMPNPMPPPPPPBPLPCKGPAODGPPEPPKBPNBPHPNPPPLPPFPPJLHINAKCALOECNKFPHFPPPHPPPLLHBHOPACOFALPPLBAMKKJDLEPNLADLPPPPBPLPLOJOAPKBDIAAIKAPPDPHPHPLPPPPBPLOOPDEAOEAAAAAFIEPNHPHPPPPPPPPAPKPLPLIAIAIAAAAAAEPLBPHPHPPMDHHBLLPCDLFAAAAAAAAAABLNBPHLHFDPPPPFPLPGJPBPPIPCPFKPPLPPGPDLJPKPMOFBOKPEIMNNMIOEMEIIMJELFNEKMFAMPPPHPLPPPPLOPPPPPHLPPLPPHPPPPPPPAAFJDCEEBIBDCABCBACAAABNBDDKIEADPPPFPLOKLPFPPKPDPHLOPLPNDPPPDPKPMMNFKGPMMOFNOINMMEKMMIMPHNNOMNIPDPHFPPKCPPHHLLPLHDLDDHHFFPPLHPLPP/EABQRAQAAAAAAAAAAAAAAAAAAAJD/2gAIAQMBAT8QSD//xAAUEQEAAAAAAAAAAAAAAAAAAACQ/9oACAECAQE/EEg//8QAKxAAAAQEBgEDBQEAAAAAAAAAABAxcQERIPAhMEBBUWFQgZGhYLHB4fHR/9oACAEBAAE/EKkeKI0LidW6k6hxONwcHG4ODicHBwcHB1Tsw43B1EPJAARmkKCPCgCKF0hGhAj6CMAAO8SHaF2gHUuJ1DqnE6l2hdo3Uu8I7LQEeIAAAQEUEfSAAQEasEZogI0ojOJV5UZUw7B4BLLkZsqMPAgAXITD87A3AfwIGgoMiR0ARoQjUPQwAfwQIIDjHsKGGNBdzMQYswkRoYUhGlAL1BgJ2OVNwdIxoF8CpgiUDoz5T9xHoMw1YFDKxEjlCwRQRoQgI0YPFN2BuxzIf5Z4FCC6IYKGLAcHPzEeHACFJ0T67bFDYS7eYKOWjwwcQPvgGmJIRthPWIgP9AUokqBb8exDWKAxjSIAe5jAqEwYghxEfcCIHwJwNQSVozwuRcONGjEGD+AEMeBRjpkYrKdIKCkSWjgyIk0mRPOrZSwmEw2EwewoP8E9ABUJHpI5BiZZUCALSJJQGwlH4rLDiIiIiI7AUVIJGJcBgAFIREeAORUmEwmGw2ZaAjIP2AiSItlAnEc0a/JHEHAihATzRRJpvsKC9eCdH9owgW8RMkQPLIAR5ID3AQToWLg4jDLuIs4kaziLuJGu4i7iLuIs4i7iRruIu4iziLuIsYhVMRYhXHfgx9wCCQ9CWCiIHaIsdBifCOTsXGkBP/8A/wD/AP8A/wD8/wD+wdMvkfd4QAAJIgphsXthBKnYuKgAUijAD4+U4zgWrkbxcd6oIynxMREiSTWw7CBnsXFAGokwWI/cCSRKSAs2LdUAgMIqhxVauRvF13SRpQEZQPgBhYj3CAQlAEPUR8CGQSWLjNACAFTD+gIIuWEHdSAWrkbxdd6oABGWwg4XIuJYgP4CRDBYuKAPiTCikfQKJQcC7OePZHqJ0Sh0dABPOD8VYv8AacJgPkHJD5ECx2rkrrulGlAgIyAQT5EkIgEI4obDECk7DEiLFxSAJ8RGwOOB/jjrQfwCoIUn9V425UVJACQlYYKAx0eKAh61clcd0CPBAAGBEEoSD3EQBRYuMsEvO2aWoRUYmbCTJY+xkrVyV13qwEZZuIpKj4iJ3FwFi4qBIfzR4SPtx6RSLcT0YCZ9wom3pVkg0HYbiYOzArVyN4uO9EEsijMMAgQsBGzn64jaFy7EQOaEWqBjh/uwiP2BwARhn7CAtlRmw5SWJyjRvo+ANQJFauSuO/Dh7wBKhe01XJUg2Awh9zofwDSOsjfQoEpy4RiVq5G8XXeqEZpDJtDqoF2IaMH8GLEOqmk0b6YPAJgUBkZC1ckLjvVEBGYTOkOgbJwcWDDGkH6EtBniEAQhKEoShKt2BYoFq5K67oQEeDASbx+UIaWTEwMzMzMzMzMzM+6LUwrjvRpA4OEhOJAQTqRxHGSDtKQQowIqDiJvJ4eXQOgPHQTyebw87jsnGig43GcTg7IRngRFvEigppru7u7u7u7u7u7u+mlKwpEfTwAAAZQypgZQyozUDMggIyhGuAICNKACPMAAAAAAAAJPpcAz6nAAAAAAAABiMhxGR9FMhRQJHYjKnIZDIcTyq7kOIyGQ4n2VEjKDIqQy00HVuoOqO1AHZCM4xr45nHTWOXOEnlQCQMHYGCQkHISQ5CMEhdgkKQzAwMHYUgYJBIECQmDsEgkEgYOwMElMhJDkJIJCkDDHGkJGSCKScshyFPRcYgSFJSAOJJQRpQAR4oIz/wD/2Q==\" data-filename=\"empty.jpg\" style=\"width: 500px;\"><br></p>', 'portfolios\\August2019\\0xO3DRWQ8nTn333WKmEd.jpg', '444', '464', '2019-08-22 09:58:50', '2019-08-22 09:58:50', NULL, 'p-p-p        [ [');
INSERT INTO `portfolios` (`id`, `title`, `body`, `image`, `seo_title`, `description`, `created_at`, `updated_at`, `deleted_at`, `slug`) VALUES
(10, 'oii', '<p><img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4QfERXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAMAAAExAAIAAAAiAAAAcgEyAAIAAAAUAAAAlIdpAAQAAAABAAAAqAAAANQABFNJAAAnEAAEU0kAACcQQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpADIwMTg6MDM6MzEgMDE6Mzg6MTcAAAOgAQADAAAAAf//AACgAgAEAAAAAQAAAfSgAwAEAAAAAQAAAZAAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAGigAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAIAAoAMBIgACEQEDEQH/3QAEAAr/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/AOxSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//9DsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJPDtofB2EwHRpPhuSUskkkkpSSSSSlJJJJKUkkkkp//9HsUkkklKSSSSUpJJIkDU8JKUkjU4WTdqxhDf3n+0f+S/6Kus6XRW3fk2bgOddjR/37/pJKc1oc521gLnfutBJ/BW6el5L9bCKh5+533N9v/SR3dSwMduzHAfHZkBs/11Uu6nkW6B4qb4MOv+f9L/NSU3fs3TsOHXEOd2L/AHH+zWP/ACCI815+G/0udQ0HQhzTLf8AOWLubJMiTyZ1V7pOQ1t7qZEWCWj+U3/zD/qElNIGRKSPn1CjKe3QNf72/B30v+mq+5viPvSUukkkkpSSSSSlJJJJKf/S7FJJJJSkkkklK417d45Ww2rBxKftAbuAAIsjc4z9GP3eVjrU6e5uRhPxn/myz+y76BSUxHVH23111sDGOeAS7V0E+XtarOTm047mssa5xcNwgTwsnHa5uXUx30m2BrviDBVnq/8AP1f1D+UJKbH7Wxf3X/5o/vS/a2L+6/8AzR/eg4fTqraG22ky8S0NMQP/ACSqZVBx73VTuAgtJ5IPikp0f2ti/uv/AM0f3pftbF/df/mj+9ZKk+uxjWOeIFo3M+ASU6n7Wxf3X/cP/JI+Nk15LXOrBAadp3CO0/xWEtTo/wDM2f1/++tSU5TfohOmb9EJ0lKSSSSUpJJJJT//0+xSSSSUpJJJJSlZ6fd6WU2fo2ew/E/Q/wCkqyRnsYPY+BSU6GXT6fUqbAPba5p/tAw7/o7VHq/8/V/VP5QrgAzKKLho5rmv+BB22N/6tU+r/wA/V/UP5QkpHjdQux6/TDQ9o+jJIIn7/aq9tr7bHWWGXO5jQeTQp4+PZkW+mzQDV7+zR/r9FaVvS8d1QbX+je0QH8z/AMYPz0lNHBxftN3uH6Jmr/M9q/8Aya1cnGryWBlkiDILdCnx6GY9TamcDk9yfznIiSnEzsduPkbGfQc0Obyf5LtSrnR/5mz+v/31qXV691LLR/g3Qfg7T/qtiXR/5mz+v/31qSnKb9EJ0zfohOkpSSSSSlJJJJKf/9TsUkkklKSSSSUpJJJJTo9Iu/nKD/Xb/wBS9LqlN1l1Zrrc8BpBI+Ko02vptbaz6TZ0PBnSCrX7XyP3GfikpVFnUcev068fSZJLSSSe7veifa+q/wCgH+af/SiH+1sj9xn4pftfI/cZ+KSkn2vqv+gH+af/AEol9r6r/oB/mn/0oh/tfI/cZ+KX7Wyf3Gfikpe27qd1bqn0e14gw0z8verHS6rK6rBYwsJfIB8NrVW/a+R+4z8Uv2vkfuM/FJTRb9EJ0gIEJJKUkkkkpSSSSSn/1exSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSU//9bsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//Z/+0P1FBob3Rvc2hvcCAzLjAAOEJJTQQlAAAAAAAQAAAAAAAAAAAAAAAAAAAAADhCSU0EOgAAAAAA5QAAABAAAAABAAAAAAALcHJpbnRPdXRwdXQAAAAFAAAAAFBzdFNib29sAQAAAABJbnRlZW51bQAAAABJbnRlAAAAAENscm0AAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAAAAAD3ByaW50UHJvb2ZTZXR1cE9iamMAAAAMAFAAcgBvAG8AZgAgAFMAZQB0AHUAcAAAAAAACnByb29mU2V0dXAAAAABAAAAAEJsdG5lbnVtAAAADGJ1aWx0aW5Qcm9vZgAAAAlwcm9vZkNNWUsAOEJJTQQ7AAAAAAItAAAAEAAAAAEAAAAAABJwcmludE91dHB1dE9wdGlvbnMAAAAXAAAAAENwdG5ib29sAAAAAABDbGJyYm9vbAAAAAAAUmdzTWJvb2wAAAAAAENybkNib29sAAAAAABDbnRDYm9vbAAAAAAATGJsc2Jvb2wAAAAAAE5ndHZib29sAAAAAABFbWxEYm9vbAAAAAAASW50cmJvb2wAAAAAAEJja2dPYmpjAAAAAQAAAAAAAFJHQkMAAAADAAAAAFJkICBkb3ViQG/gAAAAAAAAAAAAR3JuIGRvdWJAb+AAAAAAAAAAAABCbCAgZG91YkBv4AAAAAAAAAAAAEJyZFRVbnRGI1JsdAAAAAAAAAAAAAAAAEJsZCBVbnRGI1JsdAAAAAAAAAAAAAAAAFJzbHRVbnRGI1JsdEC0QAAAAAAAAAAACnZlY3RvckRhdGFib29sAQAAAABQZ1BzZW51bQAAAABQZ1BzAAAAAFBnUEMAAAAATGVmdFVudEYjUmx0AAAAAAAAAAAAAAAAVG9wIFVudEYjUmx0AAAAAAAAAAAAAAAAU2NsIFVudEYjUHJjQFkAAAAAAAAAAAAQY3JvcFdoZW5QcmludGluZ2Jvb2wAAAAADmNyb3BSZWN0Qm90dG9tbG9uZwAAAAAAAAAMY3JvcFJlY3RMZWZ0bG9uZwAAAAAAAAANY3JvcFJlY3RSaWdodGxvbmcAAAAAAAAAC2Nyb3BSZWN0VG9wbG9uZwAAAAAAOEJJTQPtAAAAAAAQAEgAAAACAAIASAAAAAIAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAWjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAAAAAAAAAIAAjhCSU0EAgAAAAAABgAAAAAAADhCSU0EMAAAAAAAAwEBAQA4QklNBC0AAAAAAAYAAQAAAAM4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADSQAAAAYAAAAAAAAAAAAAAZAAAAH0AAAACgBVAG4AdABpAHQAbABlAGQALQAxAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAH0AAABkAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAABAAAAABAAAAAAAAbnVsbAAAAAIAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAABkAAAAABSZ2h0bG9uZwAAAfQAAAAGc2xpY2VzVmxMcwAAAAFPYmpjAAAAAQAAAAAABXNsaWNlAAAAEgAAAAdzbGljZUlEbG9uZwAAAAAAAAAHZ3JvdXBJRGxvbmcAAAAAAAAABm9yaWdpbmVudW0AAAAMRVNsaWNlT3JpZ2luAAAADWF1dG9HZW5lcmF0ZWQAAAAAVHlwZWVudW0AAAAKRVNsaWNlVHlwZQAAAABJbWcgAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAZAAAAAAUmdodGxvbmcAAAH0AAAAA3VybFRFWFQAAAABAAAAAAAAbnVsbFRFWFQAAAABAAAAAAAATXNnZVRFWFQAAAABAAAAAAAGYWx0VGFnVEVYVAAAAAEAAAAAAA5jZWxsVGV4dElzSFRNTGJvb2wBAAAACGNlbGxUZXh0VEVYVAAAAAEAAAAAAAlob3J6QWxpZ25lbnVtAAAAD0VTbGljZUhvcnpBbGlnbgAAAAdkZWZhdWx0AAAACXZlcnRBbGlnbmVudW0AAAAPRVNsaWNlVmVydEFsaWduAAAAB2RlZmF1bHQAAAALYmdDb2xvclR5cGVlbnVtAAAAEUVTbGljZUJHQ29sb3JUeXBlAAAAAE5vbmUAAAAJdG9wT3V0c2V0bG9uZwAAAAAAAAAKbGVmdE91dHNldGxvbmcAAAAAAAAADGJvdHRvbU91dHNldGxvbmcAAAAAAAAAC3JpZ2h0T3V0c2V0bG9uZwAAAAAAOEJJTQQoAAAAAAAMAAAAAj/wAAAAAAAAOEJJTQQRAAAAAAABAQA4QklNBBQAAAAAAAQAAAADOEJJTQQMAAAAAAamAAAAAQAAAKAAAACAAAAB4AAA8AAAAAaKABgAAf/Y/+0ADEFkb2JlX0NNAAL/7gAOQWRvYmUAZIAAAAAB/9sAhAAMCAgICQgMCQkMEQsKCxEVDwwMDxUYExMVExMYEQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMAQ0LCw0ODRAODhAUDg4OFBQODg4OFBEMDAwMDBERDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCACAAKADASIAAhEBAxEB/90ABAAK/8QBPwAAAQUBAQEBAQEAAAAAAAAAAwABAgQFBgcICQoLAQABBQEBAQEBAQAAAAAAAAABAAIDBAUGBwgJCgsQAAEEAQMCBAIFBwYIBQMMMwEAAhEDBCESMQVBUWETInGBMgYUkaGxQiMkFVLBYjM0coLRQwclklPw4fFjczUWorKDJkSTVGRFwqN0NhfSVeJl8rOEw9N14/NGJ5SkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2N0dXZ3eHl6e3x9fn9xEAAgIBAgQEAwQFBgcHBgU1AQACEQMhMRIEQVFhcSITBTKBkRShsUIjwVLR8DMkYuFygpJDUxVjczTxJQYWorKDByY1wtJEk1SjF2RFVTZ0ZeLys4TD03Xj80aUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9ic3R1dnd4eXp7fH/9oADAMBAAIRAxEAPwDsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//Q7FJJJJSkkkklKSSSSUpJJJJSkkkklKSSTw7aHwdhMB0aT4bklLJJJJKUkkkkpSSSSSlJJJJKf//R7FJJJJSkkkklKSSSJA1PCSlJI1OFk3asYQ395/tH/kv+irrOl0Vt35Nm4DnXY0f9+/6SSnNaHOdtYC537rQSfwVunpeS/Wwioefud9zfb/0kd3UsDHbsxwHx2ZAbP9dVLup5FugeKm+DDr/n/S/zUlN37N07Dh1xDndi/wBx/s1j/wAgiPNefhv9LnUNB0Ic0y3/ADli7myTIk8mdVe6TkNbe6mRFglo/lN/8w/6hJTSBkSkj59Qoynt0DX+9vwd9L/pqvub4j70lLpJJJKUkkkkpSSSSSn/0uxSSSSUpJJJJSuNe3eOVsNqwcSn7QG7gACLI3OM/Rj93lY61OnubkYT8Z/5ss/su+gUlMR1R9t9ddbAxjngEu1dBPl7Wqzk5tOO5rLGucXDcIE8LJx2ubl1Md9Jtga74gwVZ6v/AD9X9Q/lCSmx+1sX91/+aP70v2ti/uv/AM0f3oOH06q2httpMvEtDTED/wAkqmVQce91U7gILSeSD4pKdH9rYv7r/wDNH96X7Wxf3X/5o/vWSpPrsY1jniBaNzPgElOp+1sX91/3D/ySPjZNeS1zqwQGnadwjtP8VhLU6P8AzNn9f/vrUlOU36ITpm/RCdJSkkkklKSSSSU//9PsUkkklKSSSSUpWen3ellNn6NnsPxP0P8ApKskZ7GD2PgUlOhl0+n1KmwD22uaf7QMO/6O1R6v/P1f1T+UK4AMyii4aOa5r/gQdtjf+rVPq/8AP1f1D+UJKR43ULsev0w0PaPoySCJ+/2qvba+2x1lhlzuY0Hk0KePj2ZFvps0A1e/s0f6/RWlb0vHdUG1/o3tEB/M/wDGD89JTRwcX7Td7h+iZq/zPav/AMmtXJxq8lgZZIgyC3Qp8ehmPU2pnA5Pcn85yIkpxM7Hbj5Gxn0HNDm8n+S7Uq50f+Zs/r/99al1evdSy0f4N0H4O0/6rYl0f+Zs/r/99akpym/RCdM36ITpKUkkkkpSSSSSn//U7FJJJJSkkkklKSSSSU6PSLv5yg/12/8AUvS6pTdZdWa63PAaQSPiqNNr6bW2s+k2dDwZ0gq1+18j9xn4pKVRZ1HHr9OvH0mSS0kknu73on2vqv8AoB/mn/0oh/tbI/cZ+KX7XyP3GfikpJ9r6r/oB/mn/wBKJfa+q/6Af5p/9KIf7XyP3Gfil+1sn9xn4pKXtu6ndW6p9HteIMNM/L3qx0uqyuqwWMLCXyAfDa1Vv2vkfuM/FL9r5H7jPxSU0W/RCdICBCSSlJJJJKUkkkkp/9XsUkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklP//W7FJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJSkkkklKSSSSUpJJJJT//2ThCSU0EIQAAAAAAXQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABcAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAEMAIAAyADAAMQA3AAAAAQA4QklNBAYAAAAAAAcABAAAAAEBAP/hDnlodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTM4IDc5LjE1OTgyNCwgMjAxNi8wOS8xNC0wMTowOTowMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIiB4bXA6Q3JlYXRlRGF0ZT0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgeG1wOk1vZGlmeURhdGU9IjIwMTgtMDMtMzFUMDE6Mzg6MTcrMDM6MDAiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NTY4NDRiNWMtZTJkZC03YTQ1LThlYjMtYjdhM2I2M2JlNTkzIiB4bXBNTTpEb2N1bWVudElEPSJhZG9iZTpkb2NpZDpwaG90b3Nob3A6MDMyNjAyNTQtMzQ2Yi0xMWU4LThhNzEtZGM5NDQ4NjUyMTEyIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MzZmOTIyNGMtZWQ2Ny1lNjRlLTk1YjAtYmMyNDkwMGUyMTc4IiBwaG90b3Nob3A6Q29sb3JNb2RlPSIzIiBkYzpmb3JtYXQ9ImltYWdlL2pwZWciPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJjcmVhdGVkIiBzdEV2dDppbnN0YW5jZUlEPSJ4bXAuaWlkOjM2ZjkyMjRjLWVkNjctZTY0ZS05NWIwLWJjMjQ5MDBlMjE3OCIgc3RFdnQ6d2hlbj0iMjAxOC0wMy0zMVQwMTozODoxNyswMzowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTcgKFdpbmRvd3MpIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDo1Njg0NGI1Yy1lMmRkLTdhNDUtOGViMy1iN2EzYjYzYmU1OTMiIHN0RXZ0OndoZW49IjIwMTgtMDMtMzFUMDE6Mzg6MTcrMDM6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChXaW5kb3dzKSIgc3RFdnQ6Y2hhbmdlZD0iLyIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPHBob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPHJkZjpCYWc+IDxyZGY6bGk+YWRvYmU6ZG9jaWQ6cGhvdG9zaG9wOjY1NGY4NDlkLTM0NmEtMTFlOC04YTcxLWRjOTQ0ODY1MjExMjwvcmRmOmxpPiA8L3JkZjpCYWc+IDwvcGhvdG9zaG9wOkRvY3VtZW50QW5jZXN0b3JzPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/9sAQwADAgICAgIDAgICAwMDAwQGBAQEBAQIBgYFBgkICgoJCAkJCgwPDAoLDgsJCQ0RDQ4PEBAREAoMEhMSEBMPEBAQ/9sAQwEDAwMEAwQIBAQIEAsJCxAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQ/8IAEQgBkAH0AwEiAAIRAQMRAf/EABsAAQEAAwEBAQAAAAAAAAAAAAABAgUGBAMH/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEAMQAAAB/YgSgBFAEKSgAAAlCUSgAAABKAAACUAJYKCUSgAAABFAAAAABKAAAAAJYUAAAAAAAAAAAhQCFAAAABATIJQAiiKJZQABKEoSiLBQAxyAAAAABjkGOQASwKIsFABKEoAgKCUAEoSiUAAAAEoSgACUAAJQAAASgBLCgigAAAACAoAAIoSiUAAAAGOQSgAAAAAAAABjkAEolCKAAAAAICglACAoIUAAY5ABLCywUAJQxyACVCgAAY5ABLBZRLBQAAAAgKAAAgoJZQAAAAQssKAAAmwPBl026OV3e0hymk7jhigAAASwqUSwoAAAAICgAAgKCWUAAAAY5QssKA2e6OW3XTZHh9zwnuw5nTHUe/g98dTwvdaA5gACUAJYLKJYUAAAAEBQAAEFBLBUoAAAZbU0/p6vZnObr1QNZozq9JzMPd4KAH2+A/RcNZuT85m404AIUCWFSiWFSgAAAEBQAACFASgA+h85vd2crut/T4/XD5HoxyGn0vZw4Sd6OCd4ODd6OCd6OCd6OW6ijWct3Y4N3sODw7/WHGAEKAQoAAAAICgAAAAllAG+0P1P0Fj8zzaTRU+nScv1BvvF7uGOpnEDt3EDt3EDuHDjuJxA7icQO4cOO3cQO3vD/Y/QdXtdWcYBKAEsKAAAACAoCUAIKQWCpRKOv2/Gdmcdquv48vUcv1BvuH7jhzwp9z5fftPWfm967kCpQAmZilAH2+P1P0HV7TVnGpRLCpRAqUAAAJSAmQYshjkEBccoJaShjkDv+A6M6Dgv0Dlzn+q5fqDfcP3HDmv3uk9x3DHIcB2fCmOQY2j79Xlsj84uy1xg+mJj9/l9T9B1e01ZxbIJYCmNCVRjkEoxyDFkICZBjkAEUMchJaSglD1eWH6N5PlsT846nTbk33D9xw5r8g9mw0Y+nxyGOQN/qu5MwfDP6DldH2XGE+/y+x+g6raas40CWCZDG2EyABKAMcggKAABAoJZQAADc9b+ed+aabrUm64fuOHPCAfcvp6v0n5zl0uBtPeAAGHAfoXIGo+3x+p+g6vaas40CWFSiWFAAAABAUAABBSFQUAADquV952/z+gcP3HDnhTIy7SbAAAAAAaTd/E/P/t8fqfoOr2mrONAlhUoQVKAAAAQFAABFAEoAAAJR3Xs5jpxw/ccgaj0Zj3PCPa8Q9zwj2vEPa8Q9t8I9t8I82f2+p22r2mrONAlEoJQAAAABAUEsoBAUEsCgAlCUfbvvzrojpWmG5aYblphuWmG5aYblphuWmG5aYblphuWmG51fy8JoAJYLKJYVKAAAAQFAAAIUEsoSgAAAhZYUAAAAAAAAAACWFAlhQAAAAQFAY5AEIZAlxyAAAAEuJlLiZAAMcgAAAAAAYmQEsFlEuJkAAAACAoAAAAAAAAAAEoAAAAAAAAAAAAAAAAAAAgKlCUAIKQqUJQAQpCkKgqUAJQlBCgAJQQpCgEKkMkFSgAhQAEFShBUogVAsFShKEFIWBYFQVBUFQVBUFQVBUFIVBZYVBYFQUhSFShABUoShKEFILjSpQlBCkKgsQySgBKEoQVBQEFQUhSFIVKIhklBCkKlCUgFlMaCykAmUAFlIUxthccoFgAAUQCykAAKQAFxygBZYJRKAFlhZYSqY1SA/8QAKBAAAQQBAwQDAQADAQAAAAAAAwABAgQFFBU0EBMzUBESIDAiQJBB/9oACAEBAAEFAv8AorGMpuLFnmg4+uFZULQJ7j/0NGyZCxQoqEIQbpcF3a7e2GIpUHEydw064espRgx8mCCrHawFXBOGx7JvmThx1gqDjAQTMzdTXK4EbKzd5kIXpiiu01lQ/I/YCrWDIOJihVwh6zJAbFyo4o12yb8hK4Ss7SYo2KN4vCXrIxlNw4w80HH1hfguQrCRsoaalKU3/eML96yygfof1Pyg0rJkLFDioDGJujv8Ma1aRY5Iy0dpaO2tHbWjtrR21o7a0dtaO2tHbWjtrHisgsK+Bz19JbWjtrR21o7amA4m9EMZCyDiiSQaVcHWUowYRhm/1slw/RYkv+XQ1yuFEyxJKZCFWI8CncrDlr6i19Na+otfTWvprX1Fr6i19Ra+otfTWvqLX1Fr6i19Ra+mtfTWvqJrtV3WS4fogk7RYu0mLD7j+Ph+mI8Cv8z+vx+A+dZLh+jxpu5XWSF2rHTEeBX+Z0ZpSd69iLf1D5lkuH6PGm7dhZILkB0xHgV/mIIZWCgrjrwV2lA0P08ZM35D51kuH6Nn+rhJ3hOzSYw+yZYjwK/zFifP1sRaJ/xXDKwWzUjOr+Q+dZLh+kxJf8Vlg/WaxHgV/mKmfTn+fllZsRrjd3d/xSq6YayAOzYi0pvKMoS6B86yXD9JVL2bCti71dliPAr/ADOgLZ66fLF+CFIaX4xlX5foQQzNAcINlh/BOgfOslw/S0C92srwezZxHgV/mfwrV5WSxjGEfxkR9yr0D5lkuH6XFm+hllRfYOI8Cv8AM/AASsFs0i13TM7vTrNWF+ZM0mJBxkQfMslw/Swm45wmxITgxIYuDjGr/M6hCSwSvXHWGrWMaSx1KUJfvKDeNlB86yXD9NizPMKaEWdX+Z0hCZZ1Ksao/wCeVH9q6D5lkuH6aiZw2et/mJmd3qVIVof0LBij+Pq4fOslw/T1Sd4HS/zEA8q891srdbK3ayt1srdbK3Wyt2sLdbK3awt1srdrC3Wyt2sLdbKlJ5yD51kuH6fEmZn6XK1idrSW1pLa0ltaS2tJbWktrSW1pLa0ltaS2tJbWktrSW1pLa0ltaS2tJbQqtlirJcP04CdkzO0m/1clw/UVckIYd1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqt1qrdaq3Wqrl8Bwf9hf/8QAFBEBAAAAAAAAAAAAAAAAAAAAkP/aAAgBAwEBPwFIP//EABQRAQAAAAAAAAAAAAAAAAAAAJD/2gAIAQIBAT8BSD//xAA2EAABAgMCDQMCBgMAAAAAAAABAAIDEBESISAxMjM0QUJQUWFygZEiI3EwUgQTQJChsRQkYP/aAAgBAQAGPwL9xWjGl3wqxHBiBs2nDWU2M3buO+qtZZHFy951sqjGgfE3t5V3v7cMuVY77uDV6Id/EztPdQc0RD9bkIourJwpcbxvOgvPJBzvQOeNC363c1dOj338AqQG0HEr3Xl3zJ0Am7GJCMMbN4+iEfkr/Yfa5Be2wCdXuAHNey238qjn2RwbgtijUVUa06GcThRFh2TTdtGNLvhe77aBsWnDWcClu0eAREMWB/Kq9xd8/QsnGy6QigXP/vdYoyyDrKBivtqkNgb8TqqQPwbjzK9xj+y0dyzDlmHLMOWYcsw5ZhyzDlmHLMOXrguDXCRa0VcLwsw5ZhyzDlmHKsSEWjcdmGwuVY77PIL0srzM7TnUHNEw3WrJp+mfuN8HuJ0fEv4Be1Ds8yvdeXfKf1SLHxQCFn2rPtWfas+1Z9qz7Vn2rPtWfas+1Z9qz7Vn2rPtWfas+1Z9qAEYX3SfuNkX7SrQxFOYDSo1Ig4xN/VKJ2/RQ+oSfuQNJ9TLpW9USb+qUTtOjGl3wgTBdff9aH1CT9yWdUS6VWi9l839UonaQhs1/wAKwwd+Mi9gsvF92vDDiLjiOFD6hJ+5A4ar02L9wRadafC4G6T+qUTtJ/TgRGtuAdgiEO/wvymDIycKH1CT9yvgnZvEmxxtekyf1SidpBxyXXFVEi8m/VzRJxnBvve7HKoyYl6oxpd8IseKETh9Qk/crH1oMRk9nKT+qUTtOjHVHAq6CArcR9o4P+S/VkTpFYHDmqMaB8JsX7hScPqEn7mYdYuMnXUa68J/VKJ2+jYGIXuQa0UAwXXVLb5w+oSfuYwjtyERoyD/AAn9UonbBENuLaPBVymcZUF5Ks43G8nCLTrTof2mUPqEn7mbEGyaoPGIiqLDrCiwzsvIlE7YH5cPueCsM7njIv8Aw/pP28UY0ZlCLmj6Fv7xKH1CT9zmE45H9SJG1jlEmIcMVcVZxk3k/Ut0qWGUPqEn7nbwfccCJINbeTcuLjjP1XMOsIjhcofUJP3Qx/KcTtIvY1pOK9ZDFkMWQxZDFkMWQxZDFkMWQxZDFkMWQxZDFksRe7XeofUJP3Q6Add4m97IJIK0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dy0dyhkwHXOEn7oZE4G/wCFUa/0z90iHGrVt1y2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vC2vCdCZWp/dFx/8D//xAAnEAABBAEEAQUBAQEBAAAAAAABABARISAwMUFRgUBhobHwcZFQ0f/aAAgBAQABPyHI6I/4I9DDRnGAUYQ0PChQ8KFDQoUKFCjKFzpw8KMO8ChgMBlzo84DR4xC4cYBcaHeBQwGAy50eW5YNzmMDwguHGAXDFcveR0RON95WrVvatg9q1atW3GpeV6BePUjR4xDR6GGOiMYxhowjKGjGPQx6OMY/wCJGmUMBgFS4RalyqelSp+VThU1ZcKnKC4cY8MWrVGA9GNHjEaPGoUNANxpW/OAe8uMChpW96BxGXD+c/OHlwiQNyjIMEEH3orzlxiNXzh3gUMA4Qcvzo8vJFC4AlBAAEbbkL/eM0AiBwec+MQuHGAXDHSOnxq8ALJoRZQEWHopDpJ7VAIKERQAQ462RASJiEUgHbLjEeh7wOyGAcIMUX5ykDdEItokCggQEQk9ygSCAjeLnIAuShBZAqgbIakqEdFqXz17ZDbA8ILhxgFwx0ih6IICeIBJQcAFQHISIyTsQCAAAqnIEGIneKKAYI5/8R6eXgqCFK8SEZ+WFPpIq5GmPSn0BIHKICaCY2AEASf8Stw87kCy5CLrJKEOAMXgAgv+JFFkmybJO7hENsU+46QyeQAIW4iCW8IQjjxiNaczojGbK5ASpUkA/wBJX+iB2gAGJAFo+SgGN1QZnoHdJALaSnM3SgjWm5kwxIFFBI9WnGWlpeWlEBud9kRI9rbEI8WIcCgowLoIbhSECYEwNygDCf4o+DGHFQgPbSCIiIiIjSgxPALTY2BzFoYUREMmQxJ7xlpaXl50yhgHCCDI9CghPI95QSsj7peOoXJQg8Ukx36b9/7geEFw49CdGI7YgNFFSDATvFTAGBEdJRqenBUF8ouFvFuB4dq/cXoP3HJmam/ccmagY/WSgOy30vVHEOEUZIAJjeOkFlkEgox5MwncFIAMhBneX+UYRt/gUDoKB0FA6CgdBQOgoHQUDoKB0FA6CgdBQOgoHQUDoKHQUDoKB0FA6CEbIY+piFw4wC41ChgHCiQR2gTrbYpR2pEVb/KN+z2c6IhZASYUeEOwTSBF5DQ4V9LELhx6E6IRBnsS9ixWITR7hAzYb5Rv2ezUTGxJ2BCez3LcmDDCsH+F8HYj3xCJVG+bHOhX0n5YaAzpUqVKmL01MFSCLuowEoYIRAYQyZAEFHIAxcNSFS+Ub530VL477wB9IADpUqVNym2fSVfkJ9pCHvR5CpUqVNQr6SpUuWENTU9INTUq0DkMTkkTL+AYEmRlfKN876NO6PFoAAkg2IYfKQgeyLnJZJ98LJAAJJMADkoURiwfXs00KKnQKlguglc/HjrGn0sRr2rVq1bFBWra3tBWj3MPhQIOyBBiSRE8FSI9+V8o3zvorUdoKZmRNsI4gcOxJmFNFwTsArVq+1faq2qe73cdEBkDgUDgYECEIo8GJNUCr7VvT6StWxQlcK2t7QVwjKMq1fegcQ4QYiRCDOCGrghjADifZfKN+z20QKuInQ6QwoMADgY1jaPsgZAIw59LELhxgFxqFDAYBxbOGR/Q1tpu9k+Ub9nth5QgDaQcEdCDPAGyP6pB/wDUCDJEADclAHzCLXgFiQBBlFNuQdU/CvpYhcOMBj50Dp8LcaBQxIW0OgW0cYFWIFs/Z7YAAPcmwIaH7k3LtRNHYqrcMngSmrORxgcBK5AL4BGFPpeqKGAzKmEk67YJeCU+4t9P6f6wN91EZ89HQOEGEQP8D8K+liMB6E5hhhcAG9/eHyh9MPokwA7UaHb9zlDQjgEoEGF7IkRMkzJqEWvpPz6coYDAYGYqiLn3QeVIA3y/7vZj5EP4r9pX7SvwFftK/aV+0r8pX7SvylftK/KV+0r8pX6yozJOUCBL0+lgUFw4wGqUMBokdI8d47HQRsa9FEREREREREREB5AQT0G+liNaFChQ0MUGhgHhBQiGhEAkgCpugSaQEgvLSpaSpKlSWlpeVL/SaHC4eHhBoaFGgdEYmCIOxVAisEyPTZmZmZmZmZmZmZmbDPiRHL8sNI6ZxDh+Htc5W/Ktw95cYjAINw3GF6B0QpwnKVOEuHnU4zGM5WxQwD296VvzgHtrw4VuUMA9vbWra1atWrYq2tWre0FatWrVq1atWrVq1atWrVq0JVq1atWrVq1atW4lrVq1bWgrVtatTmcRpc6oe8uMR/xp/wCRatWrVvatWrVq2trVtatWrVq1atWrVq1atWrVq1atWrVq1atW9q1atWra2tW1q1a8ryvK8q+2OPlBBWgvLFeV5x8ryvK8v5V9sFfbeV5XleV5V9rjdeW5YLjK0FxuvKK8rzoHEYBuHjOH5UOHjLjELjMLjCNA5hhj5z86nnPnX86BeFGlWdZh6yisRq00qVIUqWLyqUvKClS0hS0qmlSpUhpUqVKClpUhSpUqVIUqcZVKXlBSpaVLQVCtoYveAUFBW9qPdoeGj3whQWChoUFQVBUFQrUNywXCvGCg1tag6A5hh6/lhr//2gAMAwEAAgADAAAAEPNNHPOPKNPNNOMPMNMMMPPONHPPPOPMPPFPAPKPKPPKPPPPFNFNPPKPOAPFPAONPPPPFOPPLPPHPPDPLHFLDHOPPNNPLPPLPPPHHPPPLOPHNLLPLPPLLPPHPPPPPPNLPPPPDPKPKDHHHLDPDPBLPPOHLBPHPKLLPPPPBPOPLOPNPPKPOPEIPPOPLFPPPPHKPPHPBPLPDOHLPPPPELOOPPKPPBPHPDLPPPHPAPLPDLHPPOLGJGNFPPLPLBPHPDLPPPPPFPPPOPPNCODNDPCLHPLPPHPPPPPPDPHPILKPEDFDEKHMPPPDIOIPOEHFLPHPPPPPFPOPMDNHGAAAAAAAAECPPMPNPMPPPPPPBPLPCKGPAODGPPEPPKBPNBPHPNPPPLPPFPPJLHINAKCALOECNKFPHFPPPHPPPLLHBHOPACOFALPPLBAMKKJDLEPNLADLPPPPBPLPLOJOAPKBDIAAIKAPPDPHPHPLPPPPBPLOOPDEAOEAAAAAFIEPNHPHPPPPPPPPAPKPLPLIAIAIAAAAAAEPLBPHPHPPMDHHBLLPCDLFAAAAAAAAAABLNBPHLHFDPPPPFPLPGJPBPPIPCPFKPPLPPGPDLJPKPMOFBOKPEIMNNMIOEMEIIMJELFNEKMFAMPPPHPLPPPPLOPPPPPHLPPLPPHPPPPPPPAAFJDCEEBIBDCABCBACAAABNBDDKIEADPPPFPLOKLPFPPKPDPHLOPLPNDPPPDPKPMMNFKGPMMOFNOINMMEKMMIMPHNNOMNIPDPHFPPKCPPHHLLPLHDLDDHHFFPPLHPLPP/EABQRAQAAAAAAAAAAAAAAAAAAAJD/2gAIAQMBAT8QSD//xAAUEQEAAAAAAAAAAAAAAAAAAACQ/9oACAECAQE/EEg//8QAKxAAAAQEBgEDBQEAAAAAAAAAABAxcQERIPAhMEBBUWFQgZGhYLHB4fHR/9oACAEBAAE/EKkeKI0LidW6k6hxONwcHG4ODicHBwcHB1Tsw43B1EPJAARmkKCPCgCKF0hGhAj6CMAAO8SHaF2gHUuJ1DqnE6l2hdo3Uu8I7LQEeIAAAQEUEfSAAQEasEZogI0ojOJV5UZUw7B4BLLkZsqMPAgAXITD87A3AfwIGgoMiR0ARoQjUPQwAfwQIIDjHsKGGNBdzMQYswkRoYUhGlAL1BgJ2OVNwdIxoF8CpgiUDoz5T9xHoMw1YFDKxEjlCwRQRoQgI0YPFN2BuxzIf5Z4FCC6IYKGLAcHPzEeHACFJ0T67bFDYS7eYKOWjwwcQPvgGmJIRthPWIgP9AUokqBb8exDWKAxjSIAe5jAqEwYghxEfcCIHwJwNQSVozwuRcONGjEGD+AEMeBRjpkYrKdIKCkSWjgyIk0mRPOrZSwmEw2EwewoP8E9ABUJHpI5BiZZUCALSJJQGwlH4rLDiIiIiI7AUVIJGJcBgAFIREeAORUmEwmGw2ZaAjIP2AiSItlAnEc0a/JHEHAihATzRRJpvsKC9eCdH9owgW8RMkQPLIAR5ID3AQToWLg4jDLuIs4kaziLuJGu4i7iLuIs4i7iRruIu4iziLuIsYhVMRYhXHfgx9wCCQ9CWCiIHaIsdBifCOTsXGkBP/8A/wD/AP8A/wD8/wD+wdMvkfd4QAAJIgphsXthBKnYuKgAUijAD4+U4zgWrkbxcd6oIynxMREiSTWw7CBnsXFAGokwWI/cCSRKSAs2LdUAgMIqhxVauRvF13SRpQEZQPgBhYj3CAQlAEPUR8CGQSWLjNACAFTD+gIIuWEHdSAWrkbxdd6oABGWwg4XIuJYgP4CRDBYuKAPiTCikfQKJQcC7OePZHqJ0Sh0dABPOD8VYv8AacJgPkHJD5ECx2rkrrulGlAgIyAQT5EkIgEI4obDECk7DEiLFxSAJ8RGwOOB/jjrQfwCoIUn9V425UVJACQlYYKAx0eKAh61clcd0CPBAAGBEEoSD3EQBRYuMsEvO2aWoRUYmbCTJY+xkrVyV13qwEZZuIpKj4iJ3FwFi4qBIfzR4SPtx6RSLcT0YCZ9wom3pVkg0HYbiYOzArVyN4uO9EEsijMMAgQsBGzn64jaFy7EQOaEWqBjh/uwiP2BwARhn7CAtlRmw5SWJyjRvo+ANQJFauSuO/Dh7wBKhe01XJUg2Awh9zofwDSOsjfQoEpy4RiVq5G8XXeqEZpDJtDqoF2IaMH8GLEOqmk0b6YPAJgUBkZC1ckLjvVEBGYTOkOgbJwcWDDGkH6EtBniEAQhKEoShKt2BYoFq5K67oQEeDASbx+UIaWTEwMzMzMzMzMzM+6LUwrjvRpA4OEhOJAQTqRxHGSDtKQQowIqDiJvJ4eXQOgPHQTyebw87jsnGig43GcTg7IRngRFvEigppru7u7u7u7u7u7u+mlKwpEfTwAAAZQypgZQyozUDMggIyhGuAICNKACPMAAAAAAAAJPpcAz6nAAAAAAAABiMhxGR9FMhRQJHYjKnIZDIcTyq7kOIyGQ4n2VEjKDIqQy00HVuoOqO1AHZCM4xr45nHTWOXOEnlQCQMHYGCQkHISQ5CMEhdgkKQzAwMHYUgYJBIECQmDsEgkEgYOwMElMhJDkJIJCkDDHGkJGSCKScshyFPRcYgSFJSAOJJQRpQAR4oIz/wD/2Q==\" data-filename=\"empty.jpg\" style=\"width: 500px;\"><br></p>', 'portfolios\\August2019\\b0DYqPLzzRhhOZDHRpvg.jpg', 'ooo', 'oo', '2019-08-22 10:18:00', '2019-08-22 10:21:12', NULL, 'oiio89o'),
(11, 'jyjyj', '<p>jhhyjuyjy</p>', 'portfolios\\August2019\\NtA8JMH03nV5ymRZBmP5.png', 'jjj', 'jjj', '2019-08-23 08:21:09', '2019-08-23 08:21:09', NULL, 'jyjyj'),
(12, 'GETERTRETR', '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbgAAAGxCAMAAAAnG1NJAAAAb1BMVEX///8AAAAAAAAAAAAAAACM/PkAAAAAAAAAAAAAAAD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACd/PqL+/mL+/mZ/PqS/Pqd/PpxcXEAAACL+/kAAAARcrjeAAAAI3RSTlMANTgAMT1OnETihmDLt/jX7GrCV66QpnxyPXdjuI/J5P5fhveZwwwAABa8SURBVHhe7N3LiuNADIZRUQvbRRwnviSOZmn0/s84w0DverqLHgj6zXdeQZhS6eIyDQAAAAAAAAAAAACGP/yvwRTAh/mxjXWKD/fx2r8Gt7zgaz9O8alpOTq3lOBLfOneZ4wdfItvXQe3ZODRYnfLBV006d1SwRptHm6ZoItGs0kicHc3RQQuXiaJwI3FFBG4KCaJwHWWB9aQPORwiWa75YE5mv0ySQRutjywayYnOKLZYHmgj1Y3tzzwjFYPSwRXzSMOSzS6F0sEYzR6Wiao0ehimeAejdwSgU/R5mqZwE9e7yJwbooI3OKWCTrNXhwupy4w044b3VLBrllgxuPMBWa6OnUwRXR1epNEV2e1ZHCLBlOxZFAlC8zwSXL2HK5ZYIZrLsZh0CybYD3vZYAa8+SWDX5pXgZwaK7Fode8DGCLb1W3dLBojp7jpjlQiSp5xMEnyT1UuGbzG0VzuwrdWf+8xhzzrVg+eGkecTg0jzj0mrc4bJq3OCyahUrcNAfzUCX3GeGT5CMRKCcdN6HGfFhG6DQXUTFrVpixay4NoNesMGPTrDBjOWWFmcJJLZYR/B5f2iwluOa2B1yzwozulNdvZrxGywm75vUbveaaDjbN3ASj5iNIqJLdb7hm9xuu+T8hdJrXb8yarQHsmq0B9JqP6WDTnMzDKDm2gFIlX0FC0WwNwDVbA+g0jzi8Ttga4FcZi2WFp+ZTA1g0twZQJVsDKJNkawCuObaATnNsAbPm2AIOzdwET83cBItkboJS35qbuPt6mefLOrjbf0B5X25ShuNa40PU6z4U+yH4m3o6ZXh88m3fDi/2E1jfkpuUyxj/8PzRZ4ff5F3pctw4Dwww30jUJ1H3xRkfu7Le/xm3KhuvbFMiwCujlPHbNYnYJI5GE7z9Bkkl5pVRHRHsYKNq0jH9Hh44jZ6boJCr2cocw+yP9n3HfYPER8aWVOK80pai/2FbPsTQMsHvPEYbQ+C27YyIKkDV9VpC/H2nMWfgj5vIVp5lwsMXp7XufsX3LeP8cxPsypVrtXB0kVPxHV9xSmJe90Dt1wO/eow4t0Tc/I5NnSTorqCtRUvUGhl/yiaKeZBtUch0OlXCs0TMTUS52tkANrk/mfWU3kWB6sb64w/2HZz+ipV/kEBDwurJjap85GyK1m+Zccr0RWlOAl0RLTdBw57w0k3jdgwou3kgB/P+3igSPLnEa/LD7bY62IBkvqOn/jHCNObZ8cLguauBJHJiUrQ7vlSFQM2bQUFhdBcjnroawJjTgtdB4Q/A5it2A4GatQ0Cw3sL+XDk5ki8CUjiywX8gmP8ckTgCLVsdbS6kFIWRSFvTAgVzdL1j05Rhjh6E1jMuM24rVJLzn9WOZGNBC3xIS+9g3F8k3F6Op15/T4FHvEl34bPy4h5b1MOeo9MxWVlWX7a3kAX6x1BqYxlQ8fmRmKUGziuTFNnrQbQ/Vdbq9Zbt098oViKNbRRwCn+P9njI4Gz35y+zmZBavckP5ORoVojWEI2ofjWnbMaaONUcDckA60UkyTCWhzgILGKpgWcUik0RFHY3hRBdEc2YSRLQjGr8S0NnzXBSJQBxLMHcQ3Z2hgfDju+teGTptwaNwwHXF06A4cO5z454b2BEsNXAgvuytKzULBVDWg7kcsGQRquLIxvEPxZFhxtJHgglnC5Y5HDluccWOuhRePv7vgmgm+mhC9MgKAZf5GjlqDyv0o57p/8fIKT2e0IVzyeELHpbRs0xtjWIGf05toQ5Zut9eejmIUTbhMnkGNiTYlkQ8cr6iuHagCT0p1COx3FjG6ul+oag5qlNdMvOzQVh8XWcYPaPqnELnhBT1s0NU8FLr9XGFkGwNyhqVYOCn5Av7KEBGj/SC9Qj+c1CGo+WqnlbEzl6IKbwfN2mKcuCUD2ExaYj7HokK+XaK1xq3I0BgF5tsuokwNujSkYuKaKhAtuFU3hbZZa4lbPuH3dicZS5CF9N3RrYJPv/VZouaUhoQhc7EibBWm1IZ5MxQzWuCWBYesFvP/0jUvFIBFCGxvcUoUM/yQeq2L2FwoFxi0VQOeqE1g+i93xnYQUoJFiZ5k1DlU4oZBzKURvdpRs6pPi9xV3s2U5Moud6VxM5RDzegdtwInEA9pqAldk/qcX2F2v/iSjj1UwDk7UYSU9wIhbPRI7kQJOHQXEXgE/HIznYirVI3FbC0YGVaF9Q7ZGBvtQ52ATW+SpmMoSg6rNaStLbTHMJEEp7NXTa0WwPOSgjekcwMkwzTiV+R6wIQF5UCfDwPflipQa0ArCBS2DizxV+3v0JCj5lo05wtfKZCLBSJE+D7r1JG4DEN96CuCCEF6YuqPWTgr2pp022q9z7soCefJTqm9agDVlIU+lqeziF95ZmiMeyM0S6sB1pvsKZJEDSe363ckZgJuDEF7SKapNCgw6wQ3Qhe0oN0KTciSYu/ey4AzlQErUUTyzz0xkgxtqZuCw5LedxUpaQzSf6OYaZCcQerVE9hUDuDrtgJzonRFOoSF0uJmJVlCtV0ukfXwnFeoghFdqg1oCHL67MJOpBRAHrjkGBowjqtCBl7+dKalsXASw9FlDZqjszYlPR+g5m0PgFEyEsMF+1n93pi6qsPwd2qoE+eNWBuOLCJKIcO2xlpy42124OBj1x3ZRgYOctAmVjdEnJMQwHKHhTQc/fmgfqLWKbzKUwgsaf+BgD5qGf+CSj5qvNqK6Nd3pOF8+2PX+/PT09PLy9Hy9Xr5YGNwgCybPhcl7TdROjgAt+8Btf1oAxaD4qe7TnVP6Eba3zZ6u1yjABRy3AKPvmuQ7rQnFv5KWf8QV1qjA6WBvuD29fbKX+yfoohNeubMIwrnAWHaaLws/paw3ToUUxfq1/fudBHw7brptHjMUcBPdRQ1YFCwW6yG1zUCEXxw+cSpdTODanbXax033mLEVXiWGH7l2s+gwDQZPOZudR05wsP7AVRo5+I7b89uRPV8DAgdV2CuNYF6uxoIMaDSXYB70hdVnGcoQETio9WxZP2/6oQsIHHltwD+15IfNbqf8b5npAyxffLyMCZzWb/gFnI6bfuZi30VdIszL6yzKWjhuDAijo2zoRKkcREVnlfyIIH4Bd30zWzjgbqGnd2Difo0MCl1EnRD0sh4eW9jcrkEv2foAd9thN7c6gHCWsR9GRSfczMSX4DsgeVwMzMYSUlCSoUJt6hZH4mHUo8rmKKkjF/lKYwkRHj4CvgNajrNeMOVEE1Ckd070oaR1Stf9OnCvJHDPG3BnmpMBw+oD3KIvL1Qs1qTT3ehEHPvF48txRymwHTjqyEXmTfoYD1YBv6rFw501mK4sCDoIAKHfzhjabz0oXi7XFwZw9zDALSGTSlGtXsBBqecmglPFY7Zz46oglDSJe5AAuZNybSkllZ5EHreQx3iwCtjHvz+OVI0eqHUvCjXhCdG9D4l7/Nvl8vzGMU/gaO2osk9LPIFbdrS4NzqpxGLvER2kYgDWzp9+21P7bakJ5SsjPxNh2UVNV1/goN0pHAbSiWO1V3piQRIjrauzwWrvozZPSeWV4fQm/u1vVaz+wO1JOnuiFwcfpzjNwKFvZlKXNlmNdk///he4Ow+4p8tZXhvYtNy0ATdVk6YgjPtyiQFY578jrwWOdGqiO1ZmiHt7vcadLzo4hDfagDsyfTblhukvhED1H5cbNL7ZGMHQsZBL9t/6uDy98ewad2h9E+WeDnDpbqUBp01/AjVoC8hpLSGZmZVIEKpaMI4OHF9mLxzCG22C6bYLMPuEapiGQtv4LJ1ghnSJLkiPrp/zy8vvA27yTyqhq/2HzOl112J7CSgF7jWrgpGa3fjiDPXjASeuX30v6sAt1CTVZn85RutHVZOSqxAE8m/oURkLPAA4qH0pZhxXO5t5fHsLdk8RzGDxXp1WONqQXs3hTa/L828DThH70p2cPJ74wqsoG6sbCXUCWnHM2juztUsX5aEHudx/WznQuOno6CRgamyby1AdbHigcWuV1XXUjtPUSqmMUtfBX67RC3BO/Gjc3/QrOwTb+xSz5s3YSExwEIHoxj5IO18JqYHU3Lo6kSkvs7ZeOPfeMmFYkQw4BSXyr+IXArQ+Lr+U7Kw2LsymP9r6qLFJZiQ+z43kkmiMHpydnAJXNFbOYJvi1sDauxUQoUF7H3VTLsRu6+TE5zlVAQMhsUoYrC1yB9+keLywJStdXiyOXGKYHcCXLjz7A5c6X8qEnsrL4Wi1F7qmXYD3cn+qwCA1KlnpMvJfEhC1YQ9u6rz4mhOoHKsBENlxWkKRMnLHBbVEGIRZRyFbFBgy9QKIMo7mWUcwP1F8Aw24y5Vz4C4xQ9zgcvG0EoxfRz3A0TpcXOpPqKUJmCeSCSTZLFrJvSEDaiQf3b9whJWvAXSVjdutGkiJt8wJX9kQVcUIB9nQIqu6rKs2nRVQFdYNEibpBuNKnjlIekJMvQF3jy9BH10ejwRRMcfgzrx0DVL2qAp+1G3heFsKiw5JvQiAZKlYEzUu/9r9lThvPsDRM+bQ+n5+A18gZv0hSj3i+wtxlaHxAW5iGXq86eUduae/DnB7CXI/TnCqVPZHlglqYn56z0Nef0UVnXC7aVtjYFMiKtATSJd3u99f/jrIS0IAt9BVKt9NVsJCJF3m8BM1aArOXFZr0lSCIRBUNHVHWwJHwG3QfcHudbsE7g4czQFWdm5SoomQ1S2TaToWRAbOt2RHdyD5ZCnYvjy44aYBt0H3/5fX/7B7ed7GLngDR86R4ceCFOyYGSot9XvrIDcK0Ef+ZFIDF2sEbkPver3f79ftqPkCR6/qaOEm1xsQFT7TJIQYKT2CcQrjwrtjRLzw+kAbmVMtaN1UDsTmiHveoN1jqqAkykgqbhBc7MMMapupFtiT3Wf2etD+ljaadwG7AhWZzqHuouLmP44k5+qUK0WUHFxbHHGbNPj5jBvdgtC7EY+2lF+nwOLs3qDh4ta44fa//KBPm7ALVH7vvE3g4bhBxn6mS7V8h+9aI2WuS5Icbbjcus8IMwHb5iXP6ylXWhW8pZP+yKWuS6LqI397s+8zgjLxzeIMsNGTr4FD5OUQRDGb5eA7MkJn7AeXO0ighr1oXvfalz40p6SBgySj+APaIC9DZiUmNYqgq52UvDA2tB/Aq4txPsFZ41MFQNalmeA7NHmM/oLuuA0GnZ70mEgAAEnSdYlAOAlm/AwKNq5k3wq0WeJuP9mubh4LA7Np+gnRxv1DDRnaQ7iFZDkg6cuv0C8CgnoNxXl3JPmjgZto4FQbmveBZBrbKsuyqpDplPh6IWGoBA2MF/zT3pnsPKrEUNhGFoPCEEiYskT1/s94N7+UxW06FKaxyX++N0isKnx8bBddly3bwccQFDLA4rHCijd5H0wX5hF0OEiOufr78G+ieI/eLVwGFQ8Hces/9G6l8frbP2nQcM/JHG4/WeetYurPK9wFBc3ssWB3TzY21PTkB731bW3/6t82eGw1P17fk1IqTGojuPnswVarPuNV4Szsp3cRt+eGVLGMG9f0D7dhP6OQPbxsqYYUBjLObdxaF3G7bbEXuI6Z7PYP35Sy255803pGjujz9YiyS37dfbOvdL3JI35oR9ckcye79a+/jBzhNw50Sfiukt328LLxLfBJYaM6RMJOysTtJzqPmkRqf1fgKibymZiESaL669NfFbiM3dqIncRtLL6qGmjCDhb22yiz9ultvkx/LyGel5e4LavbcDbr7zv/Hh13E/LBLeLby99mo3IfInkIuU1MwhxbaKjoqvBdUS7x9nluo5tqsl/SJ1Tkns2oSmiNKfblFf/wqCiXuDI16plWWQxtVPvPXMfkhTyyCDLY2aj2FvjArrstKt7xYpAQff2Z6x3FbYi6KNf971rIOfrHOUZHcZvWL8o4/V3QxeFHHTFKb00aXfbmi9qo+mefH+y8nXDetRdwoOvDU62Q3dYzmO2+lt+FvoFkUMhu2w9cxfvG/yb6CjivXLjd8bfevLO1JqUvQfJB0WRu9oGbeOej9Al9DcJjqZABJsWCkukDpcJGvQ6ctMOPYVyOCfvvu873WiAF07chwsnMfAnfdxF649FGBVKuvFrwgdl0GhXwsjMzvJlOo4J0vfatkHHARAncWTEh8SBgoQTeFjZknO8VAxFtJl66YUFSR9a63ohdNyyQ5/4ZdLGTcaANf6AR2kJuJuPArMgK7WQckCr8gUFoBSdDjaBVZPN2Q40gUbSPextqxEVZSIT3ahE4cNO1/YvBbliw2s3aCW0kMWmqBDwoxjXsZuPAQ7v4YbRoqgRcaAv7mcFsHJBFvbGjMpiNA7l6y6QU58/GAelWtbdWDdyF/hmg1f/nCUwdN8s8Xgr5foKpA7hXONd2agDkR7zxkFmYOnDh9IvLu7PVAHgo7jg7NQCkUbgx3tQApEDIhGKYLdQApID+29RCDfh4b7cTimI5Vw0APqim/zxVDQDpD8opmlPVAEgOamKVYKAGcOD0ByVxoQZw4BpRiHg7NYADt1Ako4dOIaSUYaZIBv0WbaAXX4VQJKXB+DdaYPUXnNQntp+DKRx1TpJgsIwZtoBefD0M3rSFD6euUxItFiVmGN/62d+nRYkZ4lufCjZoP/fwGCEfllTOdDRAinBYfTGxKDEjNdFPIt4UJU+gKFJp64u9IqkEis+Str5YGSSVmNTXJ5VSGCSVuCn1qaAEg0WVuCn1qWBqUanE3mV94F6n2d+gD6vcKZbB4PUxvCygf15RmrPsbyBhnSY+NzFYmwdHR/9lyk8reIHlyMBNp/VUgupz4PRDxBUdDJDiyMCVcFENchN94CSc1ZoH8iMDNweD1jxUmPVyYFJ01QLFf60V4INBbgI1oC95SXNabgKy8Bdq8Z+bIHB6dyA9LzcBw4GBGy1yE5w4vW/9NMhN4Mbpi/pSGOQmyCr1LT5s4elAx+lff74Zezqw494sRyiLUugE8DDj3ikrKQ3WLcAd0Pf4SDDuhUVX5ZvOrfwG5VH2wGIhv6HA9b0i3aktlWA8qOYltcGaSnSg663r3NO4B9LK1HmFGW1e6nuuOlnFgechpROpTVQcshPtecltVByyE62VtpxdqARyhAKXzpMXhwG5UIsy+jmZABNcaBOP88d0QHqAkMvOr3cB0Qs58bYzDx+57Jqr19AwVKk6V54GgcOoVYSbVlnclEDuYR3x/Q4SypUaIfawuSlBq2ytHGymdADr0kq5+1PfEATlfgXf0z8GjKpqZe9x8Tn6mee9Hnon9I8B0iiyk9wuNQGLog09c9klhLuykH035US24K78ELjUqRbAXXnbdVO+6AxAsleCS2154ICUOw2C1nZiALz2Lc2Qzu0bSKhXvmid2diIA/IMex5K7R23wWL/AsemJil5AGM7U2RqUgl5AFKukzjdnpMPIOU46nrNhHyA9GSJEX8106mANFaDp96LXXDlbisHzq/2RrfXujC7+ZcC6IxN1/WDfWYCxogjJ4ubzARIvd2Vmy9RpIQIv/OKFHDedA6PYBB6Q9KHP1AnZAKQJawyvSNHcrvORYnFow+hH+SxdigdAic83GTlvDnvpMRStl6ISDi72k4TPAVYD+O4ZpW3QoYA6UIEMHP8kIcIXM3mQBLEUzA5APZOLPVMzsFl6dfLATJdM25Asri4CV0PaII6JTcAbsJGipkuCCLXMbkCcBU2sAhdD+SWd49pCZDkQ3KZMbkESF6FVbpcyCtA5pVTV0G8OUek/Z+oa5ZEyD1AJO2r4kduN8M0C10GID/QhQAAAAAAAAAAAAAAAPwHevLzv9CEG9UAAAAASUVORK5CYII=\" data-filename=\"Logo_TV_2015.png\" style=\"width: 440px;\"><br></p>', 'portfolios\\August2019\\kU4BXL8rGGNwlIpqR0s0.jpg', 'ะพะ', 'พะพ', '2019-08-24 00:01:06', '2019-08-24 00:01:06', NULL, 'getertretr'),
(13, 'rtr', '<p><img style=\"width: 440px;\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAbgAAAGxCAMAAAAnG1NJAAAAb1BMVEX///8AAAAAAAAAAAAAAACM/PkAAAAAAAAAAAAAAAD///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACd/PqL+/mL+/mZ/PqS/Pqd/PpxcXEAAACL+/kAAAARcrjeAAAAI3RSTlMANTgAMT1OnETihmDLt/jX7GrCV66QpnxyPXdjuI/J5P5fhveZwwwAABa8SURBVHhe7N3LiuNADIZRUQvbRRwnviSOZmn0/s84w0DverqLHgj6zXdeQZhS6eIyDQAAAAAAAAAAAACGP/yvwRTAh/mxjXWKD/fx2r8Gt7zgaz9O8alpOTq3lOBLfOneZ4wdfItvXQe3ZODRYnfLBV006d1SwRptHm6ZoItGs0kicHc3RQQuXiaJwI3FFBG4KCaJwHWWB9aQPORwiWa75YE5mv0ySQRutjywayYnOKLZYHmgj1Y3tzzwjFYPSwRXzSMOSzS6F0sEYzR6Wiao0ehimeAejdwSgU/R5mqZwE9e7yJwbooI3OKWCTrNXhwupy4w044b3VLBrllgxuPMBWa6OnUwRXR1epNEV2e1ZHCLBlOxZFAlC8zwSXL2HK5ZYIZrLsZh0CybYD3vZYAa8+SWDX5pXgZwaK7Fode8DGCLb1W3dLBojp7jpjlQiSp5xMEnyT1UuGbzG0VzuwrdWf+8xhzzrVg+eGkecTg0jzj0mrc4bJq3OCyahUrcNAfzUCX3GeGT5CMRKCcdN6HGfFhG6DQXUTFrVpixay4NoNesMGPTrDBjOWWFmcJJLZYR/B5f2iwluOa2B1yzwozulNdvZrxGywm75vUbveaaDjbN3ASj5iNIqJLdb7hm9xuu+T8hdJrXb8yarQHsmq0B9JqP6WDTnMzDKDm2gFIlX0FC0WwNwDVbA+g0jzi8Ttga4FcZi2WFp+ZTA1g0twZQJVsDKJNkawCuObaATnNsAbPm2AIOzdwET83cBItkboJS35qbuPt6mefLOrjbf0B5X25ShuNa40PU6z4U+yH4m3o6ZXh88m3fDi/2E1jfkpuUyxj/8PzRZ4ff5F3pctw4Dwww30jUJ1H3xRkfu7Le/xm3KhuvbFMiwCujlPHbNYnYJI5GE7z9Bkkl5pVRHRHsYKNq0jH9Hh44jZ6boJCr2cocw+yP9n3HfYPER8aWVOK80pai/2FbPsTQMsHvPEYbQ+C27YyIKkDV9VpC/H2nMWfgj5vIVp5lwsMXp7XufsX3LeP8cxPsypVrtXB0kVPxHV9xSmJe90Dt1wO/eow4t0Tc/I5NnSTorqCtRUvUGhl/yiaKeZBtUch0OlXCs0TMTUS52tkANrk/mfWU3kWB6sb64w/2HZz+ipV/kEBDwurJjap85GyK1m+Zccr0RWlOAl0RLTdBw57w0k3jdgwou3kgB/P+3igSPLnEa/LD7bY62IBkvqOn/jHCNObZ8cLguauBJHJiUrQ7vlSFQM2bQUFhdBcjnroawJjTgtdB4Q/A5it2A4GatQ0Cw3sL+XDk5ki8CUjiywX8gmP8ckTgCLVsdbS6kFIWRSFvTAgVzdL1j05Rhjh6E1jMuM24rVJLzn9WOZGNBC3xIS+9g3F8k3F6Op15/T4FHvEl34bPy4h5b1MOeo9MxWVlWX7a3kAX6x1BqYxlQ8fmRmKUGziuTFNnrQbQ/Vdbq9Zbt098oViKNbRRwCn+P9njI4Gz35y+zmZBavckP5ORoVojWEI2ofjWnbMaaONUcDckA60UkyTCWhzgILGKpgWcUik0RFHY3hRBdEc2YSRLQjGr8S0NnzXBSJQBxLMHcQ3Z2hgfDju+teGTptwaNwwHXF06A4cO5z454b2BEsNXAgvuytKzULBVDWg7kcsGQRquLIxvEPxZFhxtJHgglnC5Y5HDluccWOuhRePv7vgmgm+mhC9MgKAZf5GjlqDyv0o57p/8fIKT2e0IVzyeELHpbRs0xtjWIGf05toQ5Zut9eejmIUTbhMnkGNiTYlkQ8cr6iuHagCT0p1COx3FjG6ul+oag5qlNdMvOzQVh8XWcYPaPqnELnhBT1s0NU8FLr9XGFkGwNyhqVYOCn5Av7KEBGj/SC9Qj+c1CGo+WqnlbEzl6IKbwfN2mKcuCUD2ExaYj7HokK+XaK1xq3I0BgF5tsuokwNujSkYuKaKhAtuFU3hbZZa4lbPuH3dicZS5CF9N3RrYJPv/VZouaUhoQhc7EibBWm1IZ5MxQzWuCWBYesFvP/0jUvFIBFCGxvcUoUM/yQeq2L2FwoFxi0VQOeqE1g+i93xnYQUoJFiZ5k1DlU4oZBzKURvdpRs6pPi9xV3s2U5Moud6VxM5RDzegdtwInEA9pqAldk/qcX2F2v/iSjj1UwDk7UYSU9wIhbPRI7kQJOHQXEXgE/HIznYirVI3FbC0YGVaF9Q7ZGBvtQ52ATW+SpmMoSg6rNaStLbTHMJEEp7NXTa0WwPOSgjekcwMkwzTiV+R6wIQF5UCfDwPflipQa0ArCBS2DizxV+3v0JCj5lo05wtfKZCLBSJE+D7r1JG4DEN96CuCCEF6YuqPWTgr2pp022q9z7soCefJTqm9agDVlIU+lqeziF95ZmiMeyM0S6sB1pvsKZJEDSe363ckZgJuDEF7SKapNCgw6wQ3Qhe0oN0KTciSYu/ey4AzlQErUUTyzz0xkgxtqZuCw5LedxUpaQzSf6OYaZCcQerVE9hUDuDrtgJzonRFOoSF0uJmJVlCtV0ukfXwnFeoghFdqg1oCHL67MJOpBRAHrjkGBowjqtCBl7+dKalsXASw9FlDZqjszYlPR+g5m0PgFEyEsMF+1n93pi6qsPwd2qoE+eNWBuOLCJKIcO2xlpy42124OBj1x3ZRgYOctAmVjdEnJMQwHKHhTQc/fmgfqLWKbzKUwgsaf+BgD5qGf+CSj5qvNqK6Nd3pOF8+2PX+/PT09PLy9Hy9Xr5YGNwgCybPhcl7TdROjgAt+8Btf1oAxaD4qe7TnVP6Eba3zZ6u1yjABRy3AKPvmuQ7rQnFv5KWf8QV1qjA6WBvuD29fbKX+yfoohNeubMIwrnAWHaaLws/paw3ToUUxfq1/fudBHw7brptHjMUcBPdRQ1YFCwW6yG1zUCEXxw+cSpdTODanbXax033mLEVXiWGH7l2s+gwDQZPOZudR05wsP7AVRo5+I7b89uRPV8DAgdV2CuNYF6uxoIMaDSXYB70hdVnGcoQETio9WxZP2/6oQsIHHltwD+15IfNbqf8b5npAyxffLyMCZzWb/gFnI6bfuZi30VdIszL6yzKWjhuDAijo2zoRKkcREVnlfyIIH4Bd30zWzjgbqGnd2Difo0MCl1EnRD0sh4eW9jcrkEv2foAd9thN7c6gHCWsR9GRSfczMSX4DsgeVwMzMYSUlCSoUJt6hZH4mHUo8rmKKkjF/lKYwkRHj4CvgNajrNeMOVEE1Ckd070oaR1Stf9OnCvJHDPG3BnmpMBw+oD3KIvL1Qs1qTT3ehEHPvF48txRymwHTjqyEXmTfoYD1YBv6rFw501mK4sCDoIAKHfzhjabz0oXi7XFwZw9zDALSGTSlGtXsBBqecmglPFY7Zz46oglDSJe5AAuZNybSkllZ5EHreQx3iwCtjHvz+OVI0eqHUvCjXhCdG9D4l7/Nvl8vzGMU/gaO2osk9LPIFbdrS4NzqpxGLvER2kYgDWzp9+21P7bakJ5SsjPxNh2UVNV1/goN0pHAbSiWO1V3piQRIjrauzwWrvozZPSeWV4fQm/u1vVaz+wO1JOnuiFwcfpzjNwKFvZlKXNlmNdk///he4Ow+4p8tZXhvYtNy0ATdVk6YgjPtyiQFY578jrwWOdGqiO1ZmiHt7vcadLzo4hDfagDsyfTblhukvhED1H5cbNL7ZGMHQsZBL9t/6uDy98ewad2h9E+WeDnDpbqUBp01/AjVoC8hpLSGZmZVIEKpaMI4OHF9mLxzCG22C6bYLMPuEapiGQtv4LJ1ghnSJLkiPrp/zy8vvA27yTyqhq/2HzOl112J7CSgF7jWrgpGa3fjiDPXjASeuX30v6sAt1CTVZn85RutHVZOSqxAE8m/oURkLPAA4qH0pZhxXO5t5fHsLdk8RzGDxXp1WONqQXs3hTa/L828DThH70p2cPJ74wqsoG6sbCXUCWnHM2juztUsX5aEHudx/WznQuOno6CRgamyby1AdbHigcWuV1XXUjtPUSqmMUtfBX67RC3BO/Gjc3/QrOwTb+xSz5s3YSExwEIHoxj5IO18JqYHU3Lo6kSkvs7ZeOPfeMmFYkQw4BSXyr+IXArQ+Lr+U7Kw2LsymP9r6qLFJZiQ+z43kkmiMHpydnAJXNFbOYJvi1sDauxUQoUF7H3VTLsRu6+TE5zlVAQMhsUoYrC1yB9+keLywJStdXiyOXGKYHcCXLjz7A5c6X8qEnsrL4Wi1F7qmXYD3cn+qwCA1KlnpMvJfEhC1YQ9u6rz4mhOoHKsBENlxWkKRMnLHBbVEGIRZRyFbFBgy9QKIMo7mWUcwP1F8Aw24y5Vz4C4xQ9zgcvG0EoxfRz3A0TpcXOpPqKUJmCeSCSTZLFrJvSEDaiQf3b9whJWvAXSVjdutGkiJt8wJX9kQVcUIB9nQIqu6rKs2nRVQFdYNEibpBuNKnjlIekJMvQF3jy9BH10ejwRRMcfgzrx0DVL2qAp+1G3heFsKiw5JvQiAZKlYEzUu/9r9lThvPsDRM+bQ+n5+A18gZv0hSj3i+wtxlaHxAW5iGXq86eUduae/DnB7CXI/TnCqVPZHlglqYn56z0Nef0UVnXC7aVtjYFMiKtATSJd3u99f/jrIS0IAt9BVKt9NVsJCJF3m8BM1aArOXFZr0lSCIRBUNHVHWwJHwG3QfcHudbsE7g4czQFWdm5SoomQ1S2TaToWRAbOt2RHdyD5ZCnYvjy44aYBt0H3/5fX/7B7ed7GLngDR86R4ceCFOyYGSot9XvrIDcK0Ef+ZFIDF2sEbkPver3f79ftqPkCR6/qaOEm1xsQFT7TJIQYKT2CcQrjwrtjRLzw+kAbmVMtaN1UDsTmiHveoN1jqqAkykgqbhBc7MMMapupFtiT3Wf2etD+ljaadwG7AhWZzqHuouLmP44k5+qUK0WUHFxbHHGbNPj5jBvdgtC7EY+2lF+nwOLs3qDh4ta44fa//KBPm7ALVH7vvE3g4bhBxn6mS7V8h+9aI2WuS5Icbbjcus8IMwHb5iXP6ylXWhW8pZP+yKWuS6LqI397s+8zgjLxzeIMsNGTr4FD5OUQRDGb5eA7MkJn7AeXO0ighr1oXvfalz40p6SBgySj+APaIC9DZiUmNYqgq52UvDA2tB/Aq4txPsFZ41MFQNalmeA7NHmM/oLuuA0GnZ70mEgAAEnSdYlAOAlm/AwKNq5k3wq0WeJuP9mubh4LA7Np+gnRxv1DDRnaQ7iFZDkg6cuv0C8CgnoNxXl3JPmjgZto4FQbmveBZBrbKsuyqpDplPh6IWGoBA2MF/zT3pnsPKrEUNhGFoPCEEiYskT1/s94N7+UxW06FKaxyX++N0isKnx8bBddly3bwccQFDLA4rHCijd5H0wX5hF0OEiOufr78G+ieI/eLVwGFQ8Hces/9G6l8frbP2nQcM/JHG4/WeetYurPK9wFBc3ssWB3TzY21PTkB731bW3/6t82eGw1P17fk1IqTGojuPnswVarPuNV4Szsp3cRt+eGVLGMG9f0D7dhP6OQPbxsqYYUBjLObdxaF3G7bbEXuI6Z7PYP35Sy255803pGjujz9YiyS37dfbOvdL3JI35oR9ckcye79a+/jBzhNw50Sfiukt328LLxLfBJYaM6RMJOysTtJzqPmkRqf1fgKibymZiESaL669NfFbiM3dqIncRtLL6qGmjCDhb22yiz9ultvkx/LyGel5e4LavbcDbr7zv/Hh13E/LBLeLby99mo3IfInkIuU1MwhxbaKjoqvBdUS7x9nluo5tqsl/SJ1Tkns2oSmiNKfblFf/wqCiXuDI16plWWQxtVPvPXMfkhTyyCDLY2aj2FvjArrstKt7xYpAQff2Z6x3FbYi6KNf971rIOfrHOUZHcZvWL8o4/V3QxeFHHTFKb00aXfbmi9qo+mefH+y8nXDetRdwoOvDU62Q3dYzmO2+lt+FvoFkUMhu2w9cxfvG/yb6CjivXLjd8bfevLO1JqUvQfJB0WRu9oGbeOej9Al9DcJjqZABJsWCkukDpcJGvQ6ctMOPYVyOCfvvu873WiAF07chwsnMfAnfdxF649FGBVKuvFrwgdl0GhXwsjMzvJlOo4J0vfatkHHARAncWTEh8SBgoQTeFjZknO8VAxFtJl66YUFSR9a63ohdNyyQ5/4ZdLGTcaANf6AR2kJuJuPArMgK7WQckCr8gUFoBSdDjaBVZPN2Q40gUbSPextqxEVZSIT3ahE4cNO1/YvBbliw2s3aCW0kMWmqBDwoxjXsZuPAQ7v4YbRoqgRcaAv7mcFsHJBFvbGjMpiNA7l6y6QU58/GAelWtbdWDdyF/hmg1f/nCUwdN8s8Xgr5foKpA7hXONd2agDkR7zxkFmYOnDh9IvLu7PVAHgo7jg7NQCkUbgx3tQApEDIhGKYLdQApID+29RCDfh4b7cTimI5Vw0APqim/zxVDQDpD8opmlPVAEgOamKVYKAGcOD0ByVxoQZw4BpRiHg7NYADt1Ako4dOIaSUYaZIBv0WbaAXX4VQJKXB+DdaYPUXnNQntp+DKRx1TpJgsIwZtoBefD0M3rSFD6euUxItFiVmGN/62d+nRYkZ4lufCjZoP/fwGCEfllTOdDRAinBYfTGxKDEjNdFPIt4UJU+gKFJp64u9IqkEis+Str5YGSSVmNTXJ5VSGCSVuCn1qaAEg0WVuCn1qWBqUanE3mV94F6n2d+gD6vcKZbB4PUxvCygf15RmrPsbyBhnSY+NzFYmwdHR/9lyk8reIHlyMBNp/VUgupz4PRDxBUdDJDiyMCVcFENchN94CSc1ZoH8iMDNweD1jxUmPVyYFJ01QLFf60V4INBbgI1oC95SXNabgKy8Bdq8Z+bIHB6dyA9LzcBw4GBGy1yE5w4vW/9NMhN4Mbpi/pSGOQmyCr1LT5s4elAx+lff74Zezqw494sRyiLUugE8DDj3ikrKQ3WLcAd0Pf4SDDuhUVX5ZvOrfwG5VH2wGIhv6HA9b0i3aktlWA8qOYltcGaSnSg663r3NO4B9LK1HmFGW1e6nuuOlnFgechpROpTVQcshPtecltVByyE62VtpxdqARyhAKXzpMXhwG5UIsy+jmZABNcaBOP88d0QHqAkMvOr3cB0Qs58bYzDx+57Jqr19AwVKk6V54GgcOoVYSbVlnclEDuYR3x/Q4SypUaIfawuSlBq2ytHGymdADr0kq5+1PfEATlfgXf0z8GjKpqZe9x8Tn6mee9Hnon9I8B0iiyk9wuNQGLog09c9klhLuykH035US24K78ELjUqRbAXXnbdVO+6AxAsleCS2154ICUOw2C1nZiALz2Lc2Qzu0bSKhXvmid2diIA/IMex5K7R23wWL/AsemJil5AGM7U2RqUgl5AFKukzjdnpMPIOU46nrNhHyA9GSJEX8106mANFaDp96LXXDlbisHzq/2RrfXujC7+ZcC6IxN1/WDfWYCxogjJ4ubzARIvd2Vmy9RpIQIv/OKFHDedA6PYBB6Q9KHP1AnZAKQJawyvSNHcrvORYnFow+hH+SxdigdAic83GTlvDnvpMRStl6ISDi72k4TPAVYD+O4ZpW3QoYA6UIEMHP8kIcIXM3mQBLEUzA5APZOLPVMzsFl6dfLATJdM25Asri4CV0PaII6JTcAbsJGipkuCCLXMbkCcBU2sAhdD+SWd49pCZDkQ3KZMbkESF6FVbpcyCtA5pVTV0G8OUek/Z+oa5ZEyD1AJO2r4kduN8M0C10GID/QhQAAAAAAAAAAAAAAAPwHevLzv9CEG9UAAAAASUVORK5CYII=\" data-filename=\"Logo_TV_2015.png\"><br></p>', 'portfolios\\August2019\\sbyoooqqILr5dNIdHt32.jpg', 'tt', 'ttt', '2019-08-24 00:03:02', '2019-08-24 00:03:02', NULL, 'rtrrtrtrrt'),
(14, 'gfgfwewe', '<p><img src=\"http://127.0.0.1:8000/images/1604679893.jpg\" style=\"width: 430px;\">vdcvfdgfgfgf<br></p>', 'portfolios\\August2019\\BQne9NHYTsN2CPyqYPDs.png', 'gfg', 'gfg', '2019-08-24 00:36:00', '2019-08-24 07:48:55', NULL, 'gfgfwewe'),
(15, 'ii', '<p>dferferf4e4<img src=\"http://127.0.0.1:8000/images/112585199.jpg\" style=\"width: 793.49px;\"></p>', 'portfolios\\August2019\\WOXCs6LmYL451viODY5i.jpg', 'iii', 'iu', '2019-08-24 07:50:00', '2019-08-24 08:08:29', NULL, 'ii'),
(16, 'fff', '<p><img src=\"http://127.0.0.1:8000/images/943461729.jpg\" style=\"width: 25%;\"><br></p>', 'portfolios\\August2019\\RaoDIm6igdDbizbm37Wn.jpg', 'dd', 'ddd', '2019-08-24 08:42:41', '2019-08-24 08:42:41', NULL, 'fffff'),
(17, 'ddsdsdsds', '<p><img src=\"http://127.0.0.1:8000/images/1665789588.jpg\" style=\"width: 1053.49px;\"><img src=\"http://127.0.0.1:8000/images/1642629860.jpg\" style=\"width: 1053.49px;\"><br></p>', 'portfolios\\August2019\\u6Wzh6KCHy9S10vifUFz.jpg', 'dsds', 'dsdsds', '2019-08-24 08:52:00', '2019-08-24 09:05:00', NULL, 'ddsdsdsds'),
(18, 'dfdfdfdf', '<p>fdfd<img src=\"http://127.0.0.1:8000/images/1419692515.jpg\" style=\"width: 1053.49px;\"></p>', 'portfolios\\August2019\\eaadE0HfEWZxe9nd94o5.jpg', 'ff', 'ff', '2019-08-24 09:08:28', '2019-08-24 09:08:28', NULL, 'dfdfdfdf');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-08-20 05:11:33', '2019-08-20 05:11:33'),
(2, 'user', 'Normal User', '2019-08-20 05:11:33', '2019-08-20 05:11:33');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Lorem Ipsum', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', '\"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...\" \"There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...\"', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\August2019\\qn3znCuVKNOBsMPwWGWa.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Voyager', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.site_banner_image', 'Banner Image', 'settings\\August2019\\ioWABHVghFHLBybWSUAP.jpg', NULL, 'image', 6, 'Site'),
(12, 'site.navbar_bg', 'Navbar Background', '#ffffff', NULL, 'color', 7, 'Site'),
(13, 'site.navbar_text', 'Navbar Text Color', '#000000', NULL, 'color', 8, 'Site'),
(14, 'site.body_bg', 'Body Background', '#ffffff', NULL, 'color', 9, 'Site'),
(15, 'site.body_text', 'Body Text', '#000000', NULL, 'color', 10, 'Site'),
(16, 'site.viewmore_btn', 'View more Button', '#0061c1', NULL, 'color', 11, 'Site'),
(17, 'site.viewmore_text', 'View more Button Text', '#ffffff', NULL, 'color', 12, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Bew', 'admin@email.com', 'users/default.png', NULL, '$2y$10$ziwfZnsBO6pFkfPymX5XROPK5u5qdvEUQTxs53sdAu2aXz81JtEIm', 'wlivoZ7gIsI3V4rhtlLDMILXbXhmTF0cu699xdgqRFjRoHCDzDoZtRIaT5X0', NULL, '2019-08-20 05:12:03', '2019-08-20 05:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `articles_slug_unique` (`slug`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `portfolios_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
