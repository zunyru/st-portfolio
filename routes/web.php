<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /// return view('layout.home');
    $portfolios = App\Portfolio::orderBy('updated_at', 'desc')->paginate(8);
    $articles = App\Article::orderBy('updated_at', 'desc')->paginate(2);
    $customers = App\Customer::orderBy('updated_at', 'desc')->paginate(12);
    return view('layout.home', compact('portfolios', 'articles' , 'customers'));
});

Route::get('/portfolio', function () {
    /// return view('layout.home');
    $portfolios = App\Portfolio::orderBy('updated_at', 'desc')->paginate(12);
    return view('layout.portfolio', compact('portfolios'));
});

Route::get('portfolio/{slug}', function ($slug) {
    $portfolio = App\Portfolio::where('slug', '=', $slug)->firstOrFail();
    return view('layout.single-portfolio', compact('portfolio'));
});


Route::get('/article', function () {
    /// return view('layout.home');
    $articles = App\Article::orderBy('updated_at', 'desc')->paginate(12);
    return view('layout.article', compact('articles'));
});


Route::get('article/{slug}', function ($slug) {
    $article = App\Article::where('slug', '=', $slug)->firstOrFail();
    return view('layout.single-article', compact('article'));
});



Route::get('about-us', function () {
    /// return view('layout.home');
    $page = App\Page::where('slug','about-us')->firstOrFail();
    return view('layout.aboutus', compact('page'));
});

Route::get('contact-us', function () {
    /// return view('layout.home');
    $page = App\Page::where('slug','contact-us')->firstOrFail();
    return view('layout.contactus', compact('page'));
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::post('summernote','Formfield\SummernoteController@upload');
});



