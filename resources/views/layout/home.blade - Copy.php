@extends('layout.master')
@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{ setting('site.description') }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ setting('site.title') }}" />
<meta property="og:description" content="{{ setting('site.description') }}" />
<meta property="og:site_name" content="{{ setting('site.title') }}" />
<meta property="og:image" content="{{setting('site.site_banner_image')}}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ setting('site.title') }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ setting('site.title') }}" />
<meta name="twitter:description" content="{{ setting('site.description') }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{setting('site.site_banner_image')}}" />
<meta itemprop="image" content="{{setting('site.logo')}}g" />
@stop
@section('header')
<section class="pt-5">
    <!-- Heading & Description -->
    <div class="wow fadeIn">
        <!--Section heading-->
        <h2 class="h1 text-center mb-5">{{ setting("site.title") }}</h2>
        <!--Section description-->
        <!--   <p class="text-center">{{ setting('site.title') }}</p> -->
        <p class="text-center mb-5 pb-5">{{ setting("site.description") }}</p>
    </div>
</section>
@stop @section('content')
<div class="woe fadeIn">
    <h3 class="h1 text-center mb-3">Portfolio</h3>
    <div class="view-all">
        <a href="{{ url('/portfolio') }}"> View All >> </a>
    </div>
</div>
<div class="row wow fadeIn">
    @foreach($portfolios as $portfolio)

    <!--Grid column-->
    <div class="col-sm-3">
        <!--Section: Cards-->
        <div class="card">
            <a href="{{ url('portfolio/'.$portfolio->slug)}}"><img class="card-img-top hoverable"
                    src="{{ Voyager::image( $portfolio->image ) }}" />
            </a>

            <div class="card-body">
                <h5 class="card-title">{{ $portfolio->title }}</h5>
                <p class="card-text">{{ $portfolio->description }}</p>
                <a href="{{ url('portfolio/'.$portfolio->slug)}}" class="btn viewmore">View more</a>
            </div>
        </div>
    </div>
    <!--Grid column-->
    @endforeach
</div>
<div class="woe fadeIn">
    <h3 class="h3 text-center mb-3 mt-3">Article</h3>
    <div class="view-all">
        <a href="{{ url('/article') }}"> View All >> </a>
    </div>
</div>
<div class="row wow fadeIn">
    @foreach($articles as $article)

    <!--Grid column-->
    <div class="col-sm-3">
        <!--Section: Cards-->
        <div class="card">
            <a href="{{ url('portfolio/'.$article->slug)}}"><img class="card-img-top hoverable"
                    src="{{ Voyager::image( $article->image ) }}" />
            </a>

            <div class="card-body">
                <h5 class="card-title">{{ $article->title }}</h5>
                <p class="card-text">{{ $article->description }}</p>
                <a href="{{ url('article/'.$article->slug)}}" class="btn viewmore">View more</a>
            </div>
        </div>
    </div>
    <!--Grid column-->
    @endforeach
</div>
<!--Pagination-->
<!-- <nav class="d-flex justify-content-center wow fadeIn">
    {{ $portfolios->links() }}
</nav> -->
<!--Pagination-->

<!--Section: Cards-->

@endsection
