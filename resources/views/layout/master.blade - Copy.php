<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{setting('site.title')}}</title>
    @yield('seo')

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('css/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('css/style.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">

    <style>
        body {
            background-color: {
                    {
                    setting('site.body_bg')
                }
            }

            ;

            color : {
                    {
                    setting('site.body_text')
                }
            }

            ;
        }

        .btn.viewmore {
            background-color: {
                    {
                    setting('site.viewmore_btn')
                }
            }

            ;

            color : {
                    {
                    setting('site.viewmore_text')
                }
            }

            ;
        }

        .navbar.scrolling-navbar {
            background-color: {
                    {
                    setting('site.navbar_bg')
                }
            }

            ;
        }

        .navbar.navbar-light .navbar-nav .nav-item .nav-link {
            color: {
                    {
                    setting('site.navbar_text')
                }
            }

            ;
        }
    </style>

</head>
<!--Main Navigation-->

<body>
    <header>

        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-expand-lg navbar-light  scrolling-navbar">
            <div class="container">

                <!-- Brand -->
                <div class="img-logo">
                    <a class="navbar-brand waves-effect" href="{{ url('/') }}" target="_self">
                        <img src="{{ Voyager::image(setting('site.logo')) }}" class="logo">
                    </a>
                </div>

                <!-- Collapse -->
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <!-- Links -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left -->
                    <ul class="navbar-nav mr-auto">

                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="{{ url('/portfolio') }}" target="_self">Portfolio</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="{{ url('/article') }}" target="_self">Article</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="{{ url('/about-us') }}" target="_self">About Us</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link waves-effect" href="{{ url('/contact-us') }}" target="_self">Contact
                                Us</a>
                        </li>
                    </ul>

                </div>

            </div>
        </nav>
        <!-- Navbar -->

    </header>
    <!--Main Navigation-->
    <!--Main layout-->

    <main class="mt-5 pt-5">
        <div class="container">

            <!--Section: Banner-->
            <section>
                <div class="row">

                    <!-- Grid column -->
                    <div class="col-md-12 mb-3">

                        <div class="view overlay z-depth-1-half">
                            <div class="banner">
                                <img src="{{ Voyager::image(setting('site.site_banner_image')) }}" class="img-fluid"
                                    alt="">
                                <a>
                                    <div class="mask rgba-white-light"></div>
                                </a>

                            </div>
                        </div>

                    </div>
                    <!-- Grid column -->

                </div>
                <!-- Content -->
            </section>

            <!-- Heading & Description -->
            @yield('header')




            <!--Grid row-->
            @yield('content');


        </div>
        </div>
    </main>
    <!--Main layout-->
    <!--Footer-->
    <footer class="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">

        <!--Call to action-->


        <!--Copyright-->
        <div class="footer-copyright py-3">
            © 2018 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> MDBootstrap.com </a>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<!-- Initializations -->
<script type=" text/javascript"> // Animations initialization new WOW().init(); </script> </body> </html>
