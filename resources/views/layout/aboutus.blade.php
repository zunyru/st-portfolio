@extends('layout.master')
@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{ setting('site.description') }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ setting('site.title') }}" />
<meta property="og:description" content="{{ setting('site.description') }}" />
<meta property="og:site_name" content="{{ setting('site.title') }}" />
<meta property="og:image" content="{{setting('site.site_banner_image')}}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ setting('site.title') }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ setting('site.title') }}" />
<meta name="twitter:description" content="{{ setting('site.description') }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{setting('site.site_banner_image')}}" />
<meta itemprop="image" content="{{setting('site.logo')}}g" />
@stop
@section('content')
<div class="row mt-3">
    <div class="woe fadeIn">
        <h1 class="h3 text-center">เกี่ยวกับเรา</h1>
    </div>
      {{-- <div class="col-lg-6">
        <img class="img-fluid rounded mb-4" src="{{ Voyager::image( $page->image ) }}" alt="">
    </div> --}}
    <div class="col-lg-12">
       {!! $page->body !!}
   </div>
</div>
@endsection
