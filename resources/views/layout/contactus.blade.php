@extends('layout.master')
@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{ setting('site.description') }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ setting('site.title') }}" />
<meta property="og:description" content="{{ setting('site.description') }}" />
<meta property="og:site_name" content="{{ setting('site.title') }}" />
<meta property="og:image" content="{{setting('site.site_banner_image')}}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ setting('site.title') }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ setting('site.title') }}" />
<meta name="twitter:description" content="{{ setting('site.description') }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{setting('site.site_banner_image')}}" />
<meta itemprop="image" content="{{setting('site.logo')}}g" />
@stop
@section('content')
<!-- <div class="woe fadeIn">
    <h3 class="h3 text-center mb-3 mt-3">{{ $page->title }}</h3>

</div>
<hr class="mb-5">
 -->
<div class="col-md-12 col-md-offset-2">



    <!--Grid column-->



    <div class="view">
        <img src="{{ Voyager::image( $page->image ) }}" class="img-fluid" alt="smaple image">

        <div class="mask contact-us mt-5">
            <h3 class="h3 text-center mb-3 mt-3">{{ $page->title }}</h3>
            <p class="black-text">{!! $page->body !!}</p>
        </div>
    </div>

</div>




@endsection
