@extends('layout.master')

@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{  $portfolio->description }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ $portfolio->title }}" />
<meta property="og:description" content="{{  $portfolio->description }}" />
<meta property="og:site_name" content="{{ $portfolio->title }}" />
<meta property="og:image" content="{{ Voyager::image( $portfolio->image ) }}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ $portfolio->title }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ $portfolio->title }}" />
<meta name="twitter:description" content="{{ $portfolio->description }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{ Voyager::image( $portfolio->image ) }}" />
<meta itemprop="image" content="{{ Voyager::image( $portfolio->image ) }}" />
@stop

@section('content')

<div class="row single">

    <div class="col-md-12 col-md-offset-2">
        <div class="tab-content" id="nav-tabContent">
            <div class="text-center">


                <div class="woe fadeIn">
                    <h3 class="h3 text-center mb-3 mt-3">{{ $portfolio->title }}</h3>

                </div>
                @if(isset($portfolio->image))
                <img src="{{ Voyager::image( $portfolio->image ) }}" class="img-fluid z-depth-4 rounded">
                @endif
            </div>
            <div class="mt-3">
                <h1>{{ $portfolio->title }}</h1>
            </div>
            <hr class="mb-5" style="width: 100%">
            <p>{!! $portfolio->body !!}</p>
        </div>
    </div>

</div>

@endsection
