<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{setting('site.title')}}</title>
  @yield('seo')

  <!-- Bootstrap core CSS -->
  <link href="{{asset('template/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{asset('template/css/modern-business.css')}}" rel="stylesheet">
  <link href="{{asset('template/css/custom-style.css')}}" rel="stylesheet">

  <style>
    body{
      background-color: {{setting('site.body_bg')}};
      color : {{setting('site.body_text')}};
    }
    .btn.viewmore {
      background-color: {{setting('site.viewmore_btn')}};
      color : {{setting('site.viewmore_text')}};
    }
    nav.bg-custom,.page-item.active .page-link {
      background-color: {{setting('site.navbar_bg')}};
    }
    .page-item.active .page-link,
    .navbar-dark .navbar-nav .nav-link {
      color: {{setting('site.navbar_text')}};
    }
    .page-item.active .page-link{
      border-color : {{setting('site.navbar_bg')}};
    }
    
    a.title {
      color: {{ setting('site.title_link') }};
    }
    a,.page-link {
      color: {{ setting('site.link_color') }};
    }
    a:hover,.page-link:hover{
      text-decoration: none;
      color: {{ setting('site.link_hover_color') }};
    }
    .logo{
      max-height: 80px;
    }
    .navbar{
      height: 92px;
    }
    .carousel-item{
      height: 92vh;
    }
  </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-custom fixed-top">
    <div class="container">
      <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ Voyager::image(setting('site.logo')) }}" class="logo"></a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
         <li class="nav-item">
          <a class="nav-link" href="{{ url('/portfolio') }}">บริการ&ผลงาน</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/article') }}">บทความ</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/about-us') }}">เกี่ยวกับเรา</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ url('/contact-us') }}">ติดต่อเรา</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image: url('http://placehold.it/1900x1080')">
        <div class="carousel-caption d-none d-md-block">
          <h3>First Slide</h3>
          <p>This is a description for the first slide.</p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
        <div class="carousel-caption d-none d-md-block">
          <h3>Second Slide</h3>
          <p>This is a description for the second slide.</p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
        <div class="carousel-caption d-none d-md-block">
          <h3>Third Slide</h3>
          <p>This is a description for the third slide.</p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>

<!-- Page Content -->
<div class="container">

  {{-- @yield('header') --}}

  @yield('content')

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-5 bg-dark">
  <div class="container">
    <p class="m-0 text-center text-white">Copyright &copy; Your Website 2019</p>
  </div>
  <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{asset('template/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('template/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>
