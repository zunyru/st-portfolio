@extends('layout.master')

@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{  $article->description }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ $article->title }}" />
<meta property="og:description" content="{{  $article->description }}" />
<meta property="og:site_name" content="{{ $article->title }}" />
<meta property="og:image" content="{{ Voyager::image( $article->image ) }}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ $article->title }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ $article->title }}" />
<meta name="twitter:description" content="{{ $article->description }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{ Voyager::image( $article->image ) }}" />
<meta itemprop="image" content="{{ Voyager::image( $article->image ) }}" />
@stop

@section('content')

<div class="row single">

    <div class="col-md-12 col-md-offset-2">
        <div class="tab-content" id="nav-tabContent">
            <div class="text-center">


                <div class="woe fadeIn">
                    <h3 class="h3 text-center mb-3 mt-3">{{ $article->title }}</h3>

                </div>
                @if(isset($article->image))
                <img src="{{ Voyager::image( $article->image ) }}" class="img-fluid z-depth-4 rounded">
                @endif
            </div>
            <div class="mt-3">
                <h1>{{ $article->title }}</h1>
            </div>
            <hr class="mb-5" style="width: 100%">
            <p>{!! $article->body !!}</p>
        </div>
    </div>

</div>

@endsection
