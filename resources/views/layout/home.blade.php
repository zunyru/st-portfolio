@extends('layout.master')
@section('seo')
<!-- for Google by page -->
<meta name="description" content="{{ setting('site.description') }}" />

<!-- for Facebook by page -->
<meta property="fb:app_id" content="{{setting('facebook') ?? ''}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{ url('/') }}" />
<meta property="og:title" content="{{ setting('site.title') }}" />
<meta property="og:description" content="{{ setting('site.description') }}" />
<meta property="og:site_name" content="{{ setting('site.title') }}" />
<meta property="og:image" content="{{setting('site.site_banner_image')}}" />
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="600" />
<!-- <meta property="og:image:height"      content="315" /> -->
<meta property="og:image:alt" content="{{ setting('site.title') }}" />

<!-- for Twitter by page'-->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{ setting('site.title') }}" />
<meta name="twitter:description" content="{{ setting('site.description') }}" />
<meta name="twitter:creator" content="{{ setting('site.twitter') ?? '' }}" />
<meta name="twitter:site" content="{{ url('/') }}" />
<meta name="twitter:image" content="{{setting('site.site_banner_image')}}" />
<meta itemprop="image" content="{{setting('site.logo')}}g" />
@stop
@section('header')
<section class="pt-5">
    <!-- Heading & Description -->
    <div class="wow fadeIn">
        <!--Section heading-->

        <h2 class="h1 text-center mb-5">{{ setting("site.title") }}</h2>
        <!--Section description-->
        <!--   <p class="text-center">{{ setting('site.title') }}</p> -->
        <p class="text-center mb-5 pb-5">{{ setting("site.description") }}</p>
    </div>
</section>
@stop 
@section('content')
@if(isset($portfolios) && sizeof($portfolios) > 0)
<div class="woe fadeIn my-3">
    <h1 class="h3 text-left">บริการ</h1>
</div>
<div class="row wow fadeIn px-2">
    <div class="row">
        @foreach($portfolios as $portfolio)
        <div class="col-lg-3 col-sm-4 portfolio-item text-center">
            <div class="card">
              <a href="{{ url('portfolio/'.$portfolio->slug)}}">
                <img class="card-img-top portfolio" src="{{ Voyager::image( $portfolio->thumbnail('medium'))}}" alt=""></a>
            </div>
            <a class="mt-2 title" href="{{ url('portfolio/'.$portfolio->slug)}}">{{ $portfolio->title }}</a>
        </div>

        @endforeach
    </div>
</div>
<div class="view-all float-right mt-2">
    <a href="{{ url('/portfolio') }}"> ดูเพิ่มเติม </a>
</div>
@endif
@if(isset($articles) && sizeof($articles) > 0)
<div class="woe fadeIn my-3">
    <h1 class="h3 text-left">บทความ</h1>
</div>
<div class="row wow fadeIn mb-3 px-2">
    @foreach($articles as $article)
    <div class="row mt-3">
      <div class="col-md-5">
        <a href="{{ url('portfolio/'.$article->slug)}}">
            <img  class="card-img-top hoverable article" src="{{Voyager::image( $article->thumbnail('medium'))}}" />
        </a>
    </div>
    <div class="col-md-7">
        <a class="title" href="{{ url('article/'.$article->slug)}}">
            <h3>{{ $article->title }}</h3>
        </a>
        <p>{{ $article->description }}</p>
    </div>
</div>
<!--Grid column-->
@endforeach
</div>
@endif
@if(isset($customers) && sizeof($customers) > 0)
<hr>

<div class="woe fadeIn my-3">
    <h1 class="h3 text-left">ลูกค้าของเรา</h1>
</div>
<div class="row wow fadeIn mb-5 ">

    @foreach($customers as $customer)
    <div class="col-lg-2 col-sm-4 mb-4">
        <img class="img-fluid customers" src="{{ Voyager::image( $customer->logo ) }}"  alt="">
    </div>
    @endforeach  
</div>
@endif


@endsection
